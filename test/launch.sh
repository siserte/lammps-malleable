#!/bin/bash

#SBATCH --output=slurm-dmr_%j.out
#SBATCH --error=slurm-dmr_%j.err
#export OMP_NUM_THREADS=48
export LD_LIBRARY_PATH=/home/bsc15/bsc15334/apps/install/slurm/lib:/home/bsc15/bsc15334/apps/install/mpich-3.2/lib:/home/bsc15/bsc15334/projects/lammps-malleable/DMRlib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/home/bsc15/bsc15334/apps/install/gperftools/lib:$LD_LIBRARY_PATH
export PATH=/home/bsc15/bsc15334/apps/install/slurm/bin:$PATH
#export NX_ARGS="--enable-block --force-tie-master" 

NODELIST="$(scontrol show hostname $SLURM_JOB_NODELIST | paste -d, -s)"

min_procs=1
max_procs=4
pref_procs=4
size=8

start=`date +%s`

#mpiexec -iface eth0 -n $SLURM_JOB_NUM_NODES -hosts $NODELIST ../src/lmp_malleable.INTEL64 -var x $size -var y $size -var z $size -var min_procs $min_procs -var max_procs $max_procs -var pref_procs $pref_procs -in in.eam
mpiexec -iface eth0 -n $SLURM_JOB_NUM_NODES -hosts $NODELIST ../src/lmp_malleable.INTEL64 -var x $size -var y $size -var z $size -var min_procs $min_procs -var max_procs $max_procs -var pref_procs $pref_procs -in in.eam -pk omp 48 -sf omp

end=`date +%s`
runtime=$((end-start))

scontrol show job $SLURM_JOB_ID > $SLURM_JOB_NAME.info
echo "Runtime: $runtime seconds." >> $SLURM_JOB_NAME.info
