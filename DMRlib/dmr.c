/*************************************************************************************/
/*      Copyright (C) 2017 Universitat Jaume I, Castelló de la Plana, Spain.         */
/*      Written by Sergio Iserte <siserte@uji.es>                                    */
/*                                                                                   */
/*      This file is part of the Dynamic Management of Resources library (DMRlib)    */
/*                                                                                   */
/*      The DMRlib is free software: you can redistribute it and/or modify           */
/*      it under the terms of the GNU Lesser General Public License as published by  */
/*      the Free Software Foundation, either version 3 of the License, or            */
/*      (at your option) any later version.                                          */
/*                                                                                   */
/*      DMRlib is distributed in the hope that it will be useful,                    */
/*      but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*      GNU Lesser General Public License for more details.                          */
/*                                                                                   */
/*      You should have received a copy of the GNU Lesser General Public License     */
/*      along with DMRlib.  If not, see <http://www.gnu.org/licenses/>.              */
/*************************************************************************************/

#include "dmr.h"

int DMR_min, DMR_max, DMR_pref;
MPI_Comm DMR_INTERCOMM;
double DMR_refTime;

/**
 * @brief Pthread function for synchronizing processes using sockets during a 
 * shrink action. When shrinking Slurm kill all job's processes, so that, the 
 * runtime must wait for the processes being terminated before Slurm's cleanup.
 * Processes in nodes to be deallocated, must be ended. The thread in charge of 
 * that operation will wait for the all its sockets acceptance.
 * @param n_deallocs Number of processes that must connect to the created 
 * sockets (processes in the deallocated nodes).
 * @see dmr_reconfiguration
 */
void *listener(void * n_deallocs) {
    int world_size, world_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    int sockfd;
    socklen_t clilen;
    struct sockaddr_in serv_addr, cli_addr;
    int n = *((int *) n_deallocs);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        fprintf(stderr, "ERROR opening socket");
        exit(-1);
    }
    int option = 1;
    setsockopt(sockfd, SOL_SOCKET, (SO_REUSEPORT | SO_REUSEADDR), (char*) &option, sizeof (option));
    bzero((char *) &serv_addr, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(PORT);
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0) {
        fprintf(stderr, "ERROR binding socket");
        exit(-1);
    }
    if (listen(sockfd, n) < 0) {
        fprintf(stderr, "ERROR listening socket");
        exit(-1);
    }

    clilen = sizeof (cli_addr);
    int cnt = 0;
    while (cnt < n) {
        //printf("(sergio): %s(%s,%d) - start accept %d/%d on socket %d (%i len - (fam %i, addr %i, port %i)\n", __FILE__, __func__, __LINE__, cnt + 1, n, sockfd, clilen, serv_addr.sin_family, serv_addr.sin_addr.s_addr, serv_addr.sin_port);
        int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        //printf("(sergio): %s(%s,%d) - end accept (return %d) %d/%d on socket %d (%i len - (fam %i, addr %i, port %i)\n", __FILE__, __func__, __LINE__, newsockfd, cnt + 1, n, sockfd, clilen, serv_addr.sin_family, serv_addr.sin_addr.s_addr, serv_addr.sin_port);
        if (newsockfd < 0) {
            fprintf(stderr, "PID: %d [%d/%d] ERROR accepting (return %d)", getpid(), world_rank, world_size, newsockfd);
            exit(-1);
        }
        cnt++;
    }

    return 0;
}

/**
 * @brief Terminates process. If the action is a shrink, processes in the 
 * deallocated nodes must connect with the synchronization socket, before 
 * updating Slurm.
 * @param action Action identifier.
 * @param currNodes Number of nodes in the new communicator. Only used when 
 * shrinking. 
 * @param managementHost Name of the host from the operation is being managed. 
 * This value is generated in dmr_reconfigure(). Only used when shrinking. 
 * @param dependentJobId Identifier of the dependet job. This value is generated
 *  in dmr_reconfigure(). Only used when shrinking. 
 * @param socketThread Identifier of the pthread waiting for socket connection.
 * When this pthread joins, means that the synchronization has been finished.
 * This value is generated in dmr_reconfigure().Only used when shrinking. 
 */
void DMR_Detach(int action, int currNodes, char *managementHost, int dependentJobId, pthread_t socketThread) {
    int world_size, world_rank, name_len;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(processor_name, &name_len);

    MPI_Barrier(MPI_COMM_WORLD);
    if (action == 2) {
        //printf("(sergio)[%d]: %s(%s,%d) - proc: %s world: %d - current: %d\n", getpid(), __FILE__, __func__, __LINE__, processor_name, world_rank, currNodes);
        if (world_rank >= currNodes) {
            pid_t pid = fork();
            if (pid == -1) {
                fprintf(stderr, "Fork error\n");
                exit(-1);
            } else if (pid == 0) { //forked processes in the rest of the ranks are clients
                //printf("\t(sergio): %s(%s,%d) - %s %d\n", __FILE__, __func__, __LINE__, processor_name, world_rank);
                int sockfd;
                struct sockaddr_in serv_addr;
                struct hostent *server;
                sockfd = socket(AF_INET, SOCK_STREAM, 0);
                if (sockfd < 0) {
                    printf("ERROR opening socket");
                    exit(-1);
                }
                server = gethostbyname(managementHost); //managementHost);
                if (server == NULL) {
                    fprintf(stderr, "ERROR, no such host (%s)\n", managementHost);
                    exit(-1);
                }
                bzero((char *) &serv_addr, sizeof (serv_addr));
                //memset(&serv_addr, '\0', sizeof(serv_addr));
                serv_addr.sin_family = AF_INET;
                bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
                serv_addr.sin_port = htons(PORT);
                while (getppid() != 1) {
                    //printf("\t(sergio): %s(%s,%d) - %s %d - pid %d, ppid %d\n", __FILE__, __func__, __LINE__, processor_name, world_rank, getpid(), getppid());
                    sleep(1);
                }
                //printf("Before connecting from %s to %s - Socket: %d, size: %lu\n", processor_name, _managementHost, sockfd, sizeof (serv_addr));
                //printf("PID: %d [%d/%d] PPID: %d - Before connecting From %s to %s (on socket %d - %lu len - (fam %i, addr %d, port %i)\n", getpid(), world_rank, world_size, getppid(), processor_name, _managementHost, sockfd, sizeof (serv_addr), serv_addr.sin_family, serv_addr.sin_addr.s_addr, serv_addr.sin_port);
                int newconnect = connect(sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr));
                //printf("PID: %d [%d/%d] PPID: %d - After connecting (return %d) From %s to %s (on socket %d - %lu len - (fam %i, addr %d, port %i)\n", getpid(), world_rank, world_size, getppid(), newconnect, processor_name, _managementHost, sockfd, sizeof (serv_addr), serv_addr.sin_family, serv_addr.sin_addr.s_addr, serv_addr.sin_port);
                if (newconnect < 0) {
                    fprintf(stderr, "PID: %d [%d/%d] ERROR connecting (return %d) From %s to %s (on socket %d - %lu len - (fam %i, addr %d, port %i)", getpid(), world_rank, world_size, newconnect, processor_name, managementHost, sockfd, sizeof (serv_addr), serv_addr.sin_family, serv_addr.sin_addr.s_addr, serv_addr.sin_port);
                    exit(-1);
                }
                //printf("After connecting from %s to %s - Socket: %d, size: %lu\n", processor_name, _managementHost, sockfd, sizeof (serv_addr));
                close(sockfd);
                _exit(0);
            }
        } else if (world_rank == 0) {
            pthread_join(socketThread, NULL);
            job_desc_msg_t job_update;
            slurm_init_job_desc_msg(&job_update);
            job_update.job_id = dependentJobId;
            job_update.min_nodes = currNodes;
            printf("¿?¿?¿?¿?¿?[%d/%d] Deallocate last %d hosts (expand job %d, min_nodes %d)\n", world_rank, world_size, world_size - currNodes, job_update.job_id, job_update.min_nodes);
            sleep(1); //auxiliary wait for marenostrum (con 2 segundos funciona bien)
            slurm_update_job(&job_update);
            //printf("¿?¿?¿?¿?¿?[%d/%d] finish update\n", world_rank, world_size);
        }
    }
    printf("### [%d]-> %s(%s,%d) || Exiting... rank %d/%d from %s\n", getpid(), __func__, __FILE__, __LINE__, world_rank, world_size, processor_name);
    exit(0);
}

/**
 * @brief Schedules the action through the Slurm policy and set up all the 
 * necessary structures. This function also communicates with Nanox in order to
 * perform the action in the runtime.
 * @param min Minimum number of processes when shrinking.
 * @param max Maximum number of processes when expanding.
 * @param step Value used when resizing to a multiple of the current number of
 * processes.
 * @param pref Preferred number of processes running.
 * @param nnodes Number of nodes in the new communicator.
 * @param intercomm Communicator to spawn the new processes.
 * @param socketThread Identifier of the pthread waiting for socket connection.
 * This value is used to synchronize the processes in dmr_detach().Only used 
 * when shrinking. 
 * @param managementHost Name of the host from the operation is being managed. 
 * Only used when shrinking. 
 * @param dependentJobId Identifier of the dependet job. Only used when 
 * shrinking. 
 * @return Action identifier.
 * @see checkIterInhibitor
 */
int DMR_Reconfiguration(int min, int max, int step, int pref, int *nnodes, MPI_Comm *intercomm,
        pthread_t *socketThread, char **managementHost, int *dependentJobId) {
    int world_size, world_rank, name_len;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(processor_name, &name_len);
    int counter = 0, action;

    /************************************************/
    bool check = false;
    if (world_rank == 0)
        check = checkInhibitors();
    //check = checkTimeInhibitor();
    MPI_Bcast(&check, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (!check)
        return -2;
    /************************************************/

    if (world_rank == 0) {
        char *pID = getenv("SLURM_JOBID");
        uint32_t procID = atoi(pID);
        *dependentJobId = (int) procID;
        job_info_msg_t *MasterInfo;
        slurm_load_job(&MasterInfo, procID, 0);
        *nnodes = 0;
        //Waiting for stabilisation version without sincronous sockets
        while (world_size != (int) MasterInfo->job_array->num_nodes) {
            printf("waiting for stabilization... (current %d - slurm %d)\n", world_size, MasterInfo->job_array->num_nodes);
            sleep(5);
            slurm_free_job_info_msg(MasterInfo);
            slurm_load_job(&MasterInfo, procID, 0);
        }
        //Slurm chooses the action in the selection policy
        _dmr_slurm_jobinfo_t *jobinfo = (_dmr_slurm_jobinfo_t *) malloc(sizeof (_dmr_slurm_jobinfo_t));
        MasterInfo->job_array->select_jobinfo->data = jobinfo;
        jobinfo->job_id = (uint32_t) procID;
        jobinfo->min = (uint32_t) min;
        jobinfo->max = (uint32_t) max;
        jobinfo->preference = (uint32_t) pref;
        jobinfo->step = (uint32_t) step;
        jobinfo->currentNodes = (uint32_t) world_size;
        slurm_get_select_jobinfo(MasterInfo->job_array->select_jobinfo, SELECT_JOBDATA_ACTION, NULL);

        action = jobinfo->action;
        *nnodes = jobinfo->resultantNodes;

        printf("[%d]-> %s(%s,%d) || Action: %d %d - Current nodes %d, while in slurm %d\n", getpid(), __func__, __FILE__, __LINE__, jobinfo->action, jobinfo->resultantNodes, world_size, MasterInfo->job_array->num_nodes);
        //free(jobinfo);

        if (action == 2) {
            *socketThread = (pthread_t) 0;
            *managementHost = (char *) malloc(sizeof (char) * MPI_MAX_PROCESSOR_NAME);
            strcpy(*managementHost, processor_name);
        }
        free(jobinfo);
        slurm_free_job_info_msg(MasterInfo);
    }

    if (world_rank == 0) {
        //printf("(sergio): %s(%s,%d) - action %d to end with %d nodes\n", __FILE__, __func__, __LINE__, _actionRMS, *nnodes);
        switch (action) {
            case 0:
            {
                break;
            }
            case 1:
            {//expand
                job_desc_msg_t job_desc;
                job_info_msg_t *jobAux;
                resource_allocation_response_msg_t* slurm_alloc_msg_ptr;
                slurm_init_job_desc_msg(&job_desc);
                job_desc.name = (char *) "resizer";
                job_desc.dependency = (char *) malloc(sizeof (char)*64);
                sprintf(job_desc.dependency, (char *) "expand:%d", *dependentJobId);
                job_desc.user_id = getuid();
                job_desc.priority = 1000000;
                job_desc.min_nodes = *nnodes - world_size;

                if (slurm_allocate_resources(&job_desc, &slurm_alloc_msg_ptr)) {
                    slurm_perror((char *) "slurm_allocate_resources error (DMRCheckStatus)");
                    exit(1);
                }
                free(job_desc.dependency);
                *dependentJobId = slurm_alloc_msg_ptr->job_id;
                if (slurm_alloc_msg_ptr->node_list == NULL) {
                    slurm_load_job(&jobAux, *dependentJobId, 0);
                    //printf("(sergio): %s(%s,%d) - %d %s\n", __FILE__, __func__, __LINE__, _expandJobId, jobAux->job_array->nodes);
                    while (jobAux->job_array->nodes == NULL) {
                        slurm_free_job_info_msg(jobAux);
                        printf("Waiting for backfilling (check sinfo or priorities)\n");
                        counter++;
                        sleep(5);
                        if (counter == 10) {
                            printf("Backfilling not completed\n");
                            action = 0;
                            slurm_kill_job(*dependentJobId, SIGKILL, 0);
                            slurm_free_resource_allocation_response_msg(slurm_alloc_msg_ptr);
                            *dependentJobId = -1;
                            break;
                        }
                        slurm_load_job(&jobAux, *dependentJobId, 0);
                    }
                    if (action != 0)
                        slurm_free_job_info_msg(jobAux);
                }
                break;
            }
            case 2:
            {//shrink
                int n = world_size - *nnodes;
                if (pthread_create(socketThread, NULL, listener, (void *) &n) < 0) {
                    fprintf(stderr, "could not create thread");
                    exit(-1);
                }
                break;
            }
        }
    }
    //printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
    MPI_Bcast(&action, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(nnodes, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (action == 2) {
        if (world_rank > 0)
            *managementHost = (char *) malloc(sizeof (char) * MPI_MAX_PROCESSOR_NAME);
        MPI_Bcast(*managementHost, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, MPI_COMM_WORLD);
    }

    switch (action) {
        case 0:
        {
            break;
        }
        case 1:
        {//expand
            nanosExpandJob(MPI_COMM_WORLD, *nnodes, 1, intercomm, true, NULL, 0, NULL, *dependentJobId);
            break;
        }
        case 2:
        {//shrink
            nanosShrinkJob(MPI_COMM_WORLD, *nnodes, 1, intercomm, true, NULL, 0, NULL);
            break;
        }
    }
    //printf("[%d/%d](sergio): %s(%s,%d) %lu %d\n", world_rank, world_size, __FILE__, __func__, __LINE__, (unsigned long int) *socketThread, *nnodes);
    return action;
}
/*
void dmr_comm_disconnect(MPI_Comm comm) {
    int comm_size, parent_comm_size, factor, dst, i, my_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(comm, &parent_comm_size);

    if (parent_comm_size > comm_size) { //shrink
        factor = parent_comm_size / comm_size;
        for (i = 0; i < factor; i++) {
            dst = my_rank * factor + i;
            MPI_Send(&dst, 1, MPI_INT, dst, 1205, comm);
            //res = nanosMPISend(buf, count, datatype, dst, 1205, comm);
        }
    }// else { //expand
     //   nanosMPISend(buf, count, datatype, nanos::ext::MPIRemoteNode::getCurrentTaskParent(), TAG_END_TASK, comm);
    //}
    MPI_Comm_disconnect(&comm);\
}
 */

/**
 * @brief Initializes reconfiguration parameters in order to take appropriate 
 * actions and leave ready the internal structures. This function MUST be called
 *  before using the DMR_RECONFIG macro.
 * @param min Minimum number of processes when shrinking.
 * @param max Maximum number of processes when expanding.
 * @param pref Preferred number of processes running.
 */
void DMR_Set_parameters(int min, int max, int pref) {
    DMR_min = min;
    DMR_max = max;
    DMR_pref = pref;
    DMR_refTime = getWalltime();
}

void DMR_Recv_expand_default(void **data, MPI_Datatype datatype, int *size) {
    int my_rank, comm_size, parent_size, src, factor;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &parent_size);
    MPI_Status status;

    int datatype_size;
    MPI_Type_size(datatype, &datatype_size);

    factor = comm_size / parent_size;
    *size = (*size) / factor;
    *data = (double *) malloc((*size) * datatype_size);
    src = my_rank / factor;
    MPI_Recv(*data, *size, datatype, src, 0, DMR_INTERCOMM, &status);
}

void DMR_Recv_shrink_default(void ** data, MPI_Datatype datatype, int * size) {
    int my_rank, comm_size, parent_size, src, parent_data_size, i, factor;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &parent_size);
    MPI_Status status;

    //with this operation we calculate the size of each position in order to use pointer arithmetics.
    int datatype_size;
    MPI_Type_size(datatype, &datatype_size);

    parent_data_size = (*size);
    factor = parent_size / comm_size;
    (*size) = parent_data_size * factor;
    *data = (double *) malloc((*size) * datatype_size);

    char (*data_part)[datatype_size] = (*data);

    for (i = 0; i < factor; i++) {
        src = my_rank * factor + i;
        MPI_Recv(data_part + (parent_data_size * i), parent_data_size, datatype, src, 0, DMR_INTERCOMM, &status);
        //int iniPart = parent_data_size * i; //number of elems
        //int iniPartBytes = iniPart * datatype_size; //number of bytes
        //We parse the buffer to char (1 byte) and add the number of bytes equivalent to iniPart
        //MPI_Recv(((char *) (*data)) + iniPartBytes, parent_data_size, datatype, src, 0, DMR_intercomm, &status);
    }
}

/**
 * @brief Default SEND funtion for EXPAND to a number of processes multiple of the initial number. 
 * @param data
 * @param datatype
 * @param size
 * @note 
 * Check this URL to have more information about proportional redistribution:
 * http://stackoverflow.com/questions/43785904/dealing-with-pointer-arithmetic-of-a-void-data-type
 */
void DMR_Send_expand_default(void * data, MPI_Datatype datatype, int size) {
    int my_rank, comm_size, intercomm_size, localSize, factor, dst, i;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &intercomm_size);

    int datatype_size;
    MPI_Type_size(datatype, &datatype_size);

    const char (*data_part)[datatype_size] = data;

    factor = intercomm_size / comm_size;
    localSize = size / factor;
    for (i = 0; i < factor; i++) {
        dst = my_rank * factor + i;
        MPI_Send(data_part + (localSize * i), localSize, datatype, dst, 0, DMR_INTERCOMM);
        //int iniPart = localSize * i;
        //int iniPartBytes = iniPart * datatype_size;
        //MPI_Send(((char *) data) + iniPartBytes, localSize, datatype, dst, 0, DMR_intercomm);
    }
}

void DMR_Send_shrink_default(void * data, MPI_Datatype datatype, int size) {
    int my_rank, comm_size, intercomm_size, factor, dst;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &intercomm_size);

    factor = comm_size / intercomm_size;
    dst = my_rank / factor;
    MPI_Send(data, size, datatype, dst, 0, DMR_INTERCOMM);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void DMR_Recv_expand_blockcyclic(void **data, MPI_Datatype datatype, int nBlocks, int blockSize) {
    int my_rank, comm_size, parent_size, src, i;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &parent_size);
    MPI_Status status;

    int datatype_size;
    MPI_Type_size(datatype, &datatype_size);

    int rest = nBlocks % comm_size;
    int nBlocksPerProc = nBlocks / comm_size;
    if (my_rank < rest)
        nBlocksPerProc++;
    //printf("(sergio): %s(%s,%d) nBlocks: %d, blocks per proc: %d, size of block: %d\n", __FILE__, __func__, __LINE__, nBlocks, nBlocksPerProc, blockSize);
    *data = malloc(blockSize * nBlocksPerProc * datatype_size);
    char (*data_part)[datatype_size] = (*data);

    src = my_rank % parent_size;
    for (i = 0; i < nBlocksPerProc; i++) {
        //printf("(sergio): %s(%s,%d) Proc %d recv from %d\n", __FILE__, __func__, __LINE__, my_rank, src);
        MPI_Recv(data_part + (i * blockSize), blockSize, datatype, src, 0, DMR_INTERCOMM, &status);
    }
    //MPI_Recv(*data, *size, datatype, src, 0, DMR_intercomm, &status);
}

void DMR_Recv_shrink_blockcyclic(void ** data, MPI_Datatype datatype, int nBlocks, int blockSize) {
    int my_rank, comm_size, parent_size, i;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &parent_size);
    MPI_Status status;

    int datatype_size;
    MPI_Type_size(datatype, &datatype_size);

    int rest = nBlocks % comm_size;
    int nBlocksPerProc = nBlocks / comm_size;
    if (my_rank < rest)
        nBlocksPerProc++;
    //printf("(sergio): %s(%s,%d) nBlocks: %d, blocks per proc: %d, size of block: %d\n", __FILE__, __func__, __LINE__, nBlocks, nBlocksPerProc, blockSize);
    *data = malloc(blockSize * nBlocksPerProc * datatype_size);
    char (*data_part)[datatype_size] = (*data);

    for (i = 0; i < nBlocksPerProc; i++) {
        int src = ((i * comm_size) % parent_size) + my_rank;
        //printf("(sergio): %s(%s,%d) Proc %d recv from %d\n", __FILE__, __func__, __LINE__, my_rank, src);
        MPI_Recv(data_part + (i * blockSize), blockSize, datatype, src, 0, DMR_INTERCOMM, &status);
    }
}

void DMR_Send_expand_blockcyclic(void * data, MPI_Datatype datatype, int nBlocks, int blockSize) {
    int my_rank, comm_size, intercomm_size, dst, i;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &intercomm_size);

    int datatype_size;
    MPI_Type_size(datatype, &datatype_size);

    const char (*data_part)[datatype_size] = data;
    int rest = nBlocks % comm_size;
    int nBlocksPerProc = nBlocks / comm_size;
    if (my_rank < rest)
        nBlocksPerProc++;
    for (i = 0; i < nBlocksPerProc; i++) {
        dst = ((i * comm_size) % intercomm_size) + my_rank;
        //printf("(sergio): %s(%s,%d) Proc %d send to %d (id: %d)\n", __FILE__, __func__, __LINE__, my_rank, dst, i);
        MPI_Send(data_part + (i * blockSize), blockSize, datatype, dst, 0, DMR_INTERCOMM);
    }
    /*
        for (i = 0; i < factor; i++) {
            dst = my_rank * factor + i;
            MPI_Send(data_part + (localSize * i), localSize, datatype, dst, 0, DMR_intercomm);
        }*/
}

void DMR_Send_shrink_blockcyclic(void * data, MPI_Datatype datatype, int nBlocks, int blockSize) {
    int my_rank, comm_size, intercomm_size, dst, i;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &intercomm_size);

    int datatype_size;
    MPI_Type_size(datatype, &datatype_size);

    const char (*data_part)[datatype_size] = data;
    int rest = nBlocks % comm_size;
    int nBlocksPerProc = nBlocks / comm_size;
    if (my_rank < rest)
        nBlocksPerProc++;

    dst = my_rank % intercomm_size;
    for (i = 0; i < nBlocksPerProc; i++) {
        //printf("(sergio): %s(%s,%d) Proc %d send to %d (id: %d)\n", __FILE__, __func__, __LINE__, my_rank, dst, i);
        MPI_Send(data_part + (i * blockSize), blockSize, datatype, dst, 0, DMR_INTERCOMM);
    }

}