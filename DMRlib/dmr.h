/*************************************************************************************/
/*      Copyright (C) 2017 Universitat Jaume I, Castelló de la Plana, Spain.         */
/*      Written by Sergio Iserte <siserte@uji.es>                                    */
/*                                                                                   */
/*      This file is part of the Dynamic Management of Resources library (DMRlib)    */
/*                                                                                   */
/*      The DMRlib is free software: you can redistribute it and/or modify           */
/*      it under the terms of the GNU Lesser General Public License as published by  */
/*      the Free Software Foundation, either version 3 of the License, or            */
/*      (at your option) any later version.                                          */
/*                                                                                   */
/*      DMRlib is distributed in the hope that it will be useful,                    */
/*      but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*      GNU Lesser General Public License for more details.                          */
/*                                                                                   */
/*      You should have received a copy of the GNU Lesser General Public License     */
/*      along with DMRlib.  If not, see <http://www.gnu.org/licenses/>.              */
/*************************************************************************************/

/**
 * @file dmr.h
 * @version 1.0
 * @author Sergio Iserte
 * @date 4 May 2017
 * @brief Dynamic Management of Resources library (DMRlib).
 */

/************/
/* INCLUDES */
/************/
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h>
#include <readline/history.h>

#include "mpi.h"
//#include "nanos-mpi.h"
#include "slurm/slurm.h"

/************/
/************/
/************/

/***********/
/* DEFINES */
/***********/

#define STR_SIZE 2048           /**< @brief Standard size for strings. */
#define PORT 8262               /**< @brief Communication port for sockets when shrinking. */
#undef SO_REUSEPORT             /**< @brief Clear the macro because it may be already defined. */
#define SO_REUSEPORT 256        /**< @brief Number of connections accepted by the sockets. */

int _actionRMS = -1;

/**
 * @brief Macro for reconfiguring the execution.
 * @see dmr_reconfiguration
 * @see dmr_detach
 */
#define DMR_RECONFIG(DMR_compute, DMR_send_expand, DMR_recv_expand, DMR_send_shrink, DMR_recv_shrink) {\
    int DMR_comm_size, DMR_parent_size;\
    MPI_Comm_size(MPI_COMM_WORLD, &DMR_comm_size);\
    MPI_Comm_get_parent(&DMR_INTERCOMM);\
    if (DMR_INTERCOMM != MPI_COMM_NULL) {\
        MPI_Comm_remote_size(DMR_INTERCOMM, &DMR_parent_size);\
        if (DMR_comm_size > DMR_parent_size) { DMR_recv_expand; } else { DMR_recv_shrink; }\
        MPI_Comm_disconnect(&DMR_INTERCOMM);\
    } else {\
        int DMR_action, DMR_nnodes, DMR_dependentJobId;\
        char *DMR_managementHost;\
        pthread_t DMR_socketThread;\
        DMR_action = DMR_Reconfiguration(DMR_min, DMR_max, 2, DMR_pref, &DMR_nnodes, &DMR_INTERCOMM, &DMR_socketThread, &DMR_managementHost, &DMR_dependentJobId);\
        if (DMR_action){\
            int DMR_i, DMR_factor, DMR_dst, DMR_my_rank;\
            MPI_Comm_rank(MPI_COMM_WORLD, &DMR_my_rank);\
            if (DMR_action == 1) {\
                DMR_send_expand;\
                DMR_Detach(DMR_action, DMR_nnodes, DMR_managementHost, DMR_dependentJobId, DMR_socketThread);\
            } else if (DMR_action == 2) {\
                DMR_send_shrink;\
                DMR_Detach(DMR_action, DMR_nnodes, DMR_managementHost, DMR_dependentJobId, DMR_socketThread);\
            }\
        }\
    }\
}

/***********/
/***********/
/***********/

/***************/
/* DATASTRUCTS */
/***************/

/**
 * @brief Struct to send/receive information with Slurm. 
 */
typedef struct select_jobinfo {
    uint32_t job_id; /**< @brief IN Job identifier. */
    uint32_t action; /**< @brief OUT Action identifier. */
    uint32_t min; /**< @brief IN Minimum number of processes. */
    uint32_t max; /**< @brief IN Maximum number of processes. */
    uint32_t preference; /**< @brief IN Preferred number of processes. */
    uint32_t step; /**< @brief IN Multiple of number of processes. */
    uint32_t currentNodes; /**< @brief IN Current number or processes. */
    uint32_t resultantNodes; /**< @brief OUT Resultant number of processes. */
    char hostlist[STR_SIZE]; /**< @brief IN List of hosts currently allocated. This is used to select the manager host for shrinking. */
} _dmr_slurm_jobinfo_t;
/***************/
/***************/
/***************/

/********************/
/* GLOBAL VARIABLES */
/********************/
extern int DMR_min; /**< @brief Minimum number of processes. */
extern int DMR_max; /**< @brief Maximum number of processes. */
extern int DMR_pref; /**< @brief Preferred number of processes. */
extern MPI_Comm DMR_INTERCOMM; /**< @brief Handler of the spawned communicator. */
extern double DMR_refTime; /**< @brief Reference time for checking the inhibition period. */
/********************/
/********************/

/********************/


double getWalltime(void) {
    struct timeval val;
    struct timezone zone;
    gettimeofday(&val, &zone);
    return (double) val.tv_sec + (double) val.tv_usec * 1e-6;
}

int getPeriodEnv(void) {
    int val = 0;
    char *var = getenv("DMR_SCHED_PERIOD");
    if (var) {
        val = atoi(var);
        if (val < 0)
            val = 0;
    }
    return val;
}

bool checkTimeInhibitor(int period) {
    bool check = true;
    double curr_walltime = getWalltime();

    if (period) {
        if (((int) (curr_walltime - DMR_refTime)) < period) {
            //printf("Scheduling inhibited -> Time(%d/%d)\n", (int) (curr_walltime - DMR_refTime), period);
            check = false;
        }
    }
    if (check)
        DMR_refTime = getWalltime();

    return check;
}

int getIterationEnv(void) {
    int val = 0;
    char *var = getenv("DMR_SCHED_ITERATION");
    if (var) {
        val = atoi(var);
        if (val < 0)
            val = 0;
    }
    return val;
}

double getReferenceIteration(void) {
    static int iter = 0;
    return iter++;
}

bool checkIterInhibitor(int refIter) {
    bool check = true;
    int currIter = getReferenceIteration();

    if (refIter)
        if ((currIter % refIter) != 0) {
            //printf("Scheduling inhibited -> Iteration(%d/%d)\n", currIter % refIter, refIter);
            check = false;
        }
    return check;
}

/**
 * @brief Checks if a scheduling must be avoided by number of iterations or time
 *  period. To enable this feature, the environment variables
 * $DMR_SCHED_ITERATION and/or $DMR_SCHED_PERIOD must be set with the number of 
 * iteration to avoid the scheduling or the number of seconds of inhibition, 
 * respectively. The iteration inhibition has priority.
 * @return Perform the scheduling or not (false -> inhibit). 
 */
bool checkInhibitors(void) {
    bool check = true;
    int envIter = getIterationEnv();
    int envPeriod = getPeriodEnv();
    if (envIter) {
        check = checkIterInhibitor(envIter);
    }
    if (check && envPeriod) {
        check = checkTimeInhibitor(envPeriod);
    }
    return check;
}

int DMR_Reconfiguration(int min, int max, int step, int pref, int *nnodes, MPI_Comm *intercomm,
        pthread_t *socketThread, char **managementHost, int *dependentJobId);
void DMR_Detach(int action, int currNodes, char *managementHost, int dependentJobId, pthread_t socketThread);
void DMR_Set_parameters(int min, int max, int pref);

void DMR_Send_expand_default(void *data, MPI_Datatype datatype, int size);
void DMR_Recv_expand_default(void **data, MPI_Datatype datatype, int *size);
void DMR_Send_shrink_default(void *data, MPI_Datatype datatype, int size);
void DMR_Recv_shrink_default(void **data, MPI_Datatype datatype, int *size);

void DMR_Recv_expand_blockcyclic(void **data, MPI_Datatype datatype, int nBlocks, int blockSize);
void DMR_Recv_shrink_blockcyclic(void ** data, MPI_Datatype datatype, int nBlocks, int blockSize);
void DMR_Send_expand_blockcyclic(void * data, MPI_Datatype datatype, int nBlocks, int blockSize);
void DMR_Send_shrink_blockcyclic(void * data, MPI_Datatype datatype, int nBlocks, int blockSize);

