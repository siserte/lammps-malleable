/*************************************************************************************/
/*      Copyright (C) 2017 Universitat Jaume I, Castelló de la Plana, Spain.         */
/*      Written by Sergio Iserte <siserte@uji.es>                                    */
/*                                                                                   */
/*      This file is part of the Dynamic Management of Resources library (DMRlib)    */
/*                                                                                   */
/*      The DMRlib is free software: you can redistribute it and/or modify           */
/*      it under the terms of the GNU Lesser General Public License as published by  */
/*      the Free Software Foundation, either version 3 of the License, or            */
/*      (at your option) any later version.                                          */
/*                                                                                   */
/*      DMRlib is distributed in the hope that it will be useful,                    */
/*      but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*      GNU Lesser General Public License for more details.                          */
/*                                                                                   */
/*      You should have received a copy of the GNU Lesser General Public License     */
/*      along with DMRlib.  If not, see <http://www.gnu.org/licenses/>.              */
/*************************************************************************************/

#include "dmr.h"
#include <mpi.h>

#include <vector>
#include <string>
#include <list>
#include <cassert>
#include <sstream>
#include <fstream>

int DMR_min, DMR_max, DMR_pref;
MPI_Comm DMR_INTERCOMM;
double DMR_refTime;

int _currNodes = -1;

/**
 * @brief Pthread function for synchronizing processes using sockets during a
 * shrink action. When shrinking Slurm kill all job's processes, so that, the
 * runtime must wait for the processes being terminated before Slurm's cleanup.
 * Processes in nodes to be deallocated, must be ended. The thread in charge of
 * that operation will wait for the all its sockets acceptance.
 * @param n_deallocs Number of processes that must connect to the created
 * sockets (processes in the deallocated nodes).
 * @see dmr_reconfiguration
 */
void *listener(void * n_deallocs) {
    int world_size, world_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    int sockfd;
    socklen_t clilen;
    struct sockaddr_in serv_addr, cli_addr;
    int n = *((int *) n_deallocs);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        fprintf(stderr, "ERROR opening socket");
        exit(-1);
    }
    int option = 1;
    setsockopt(sockfd, SOL_SOCKET, (SO_REUSEPORT | SO_REUSEADDR), (char*) &option, sizeof (option));
    bzero((char *) &serv_addr, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(PORT);
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0) {
        fprintf(stderr, "ERROR binding socket");
        exit(-1);
    }
    if (listen(sockfd, n) < 0) {
        fprintf(stderr, "ERROR listening socket");
        exit(-1);
    }
    clilen = sizeof (cli_addr);
    int cnt = 0;
    while (cnt < n) {
        printf("(sergio): %s(%s,%d) - %d/%d\n", __FILE__, __func__, __LINE__, cnt, n);
        //printf("(sergio): %s(%s,%d) - start accept %d/%d on socket %d (%i len - (fam %i, addr %i, port %i)\n", __FILE__, __func__, __LINE__, cnt + 1, n, sockfd, clilen, serv_addr.sin_family, serv_addr.sin_addr.s_addr, serv_addr.sin_port);
        int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        printf("(sergio): %s(%s,%d) - %d/%d\n", __FILE__, __func__, __LINE__, cnt, n);
        //printf("(sergio): %s(%s,%d) - end accept (return %d) %d/%d on socket %d (%i len - (fam %i, addr %i, port %i)\n", __FILE__, __func__, __LINE__, newsockfd, cnt + 1, n, sockfd, clilen, serv_addr.sin_family, serv_addr.sin_addr.s_addr, serv_addr.sin_port);
        if (newsockfd < 0) {
            fprintf(stderr, "PID: %d [%d/%d] ERROR accepting (return %d)", getpid(), world_rank, world_size, newsockfd);
            exit(-1);
        }
        printf("(sergio): %s(%s,%d) - %d/%d\n", __FILE__, __func__, __LINE__, cnt, n);
        cnt++;
    }

    return 0;
}

/**
 * @brief Terminates process. If the action is a shrink, processes in the
 * deallocated nodes must connect with the synchronization socket, before
 * updating Slurm.
 * @param action Action identifier.
 * @param currNodes Number of nodes in the new communicator. Only used when
 * shrinking.
 * @param managementHost Name of the host from the operation is being managed.
 * This value is generated in dmr_reconfigure(). Only used when shrinking.
 * @param dependentJobId Identifier of the dependet job. This value is generated
 *  in dmr_reconfigure(). Only used when shrinking.
 * @param socketThread Identifier of the pthread waiting for socket connection.
 * When this pthread joins, means that the synchronization has been finished.
 * This value is generated in dmr_reconfigure().Only used when shrinking.
 */

void DMR_Detach(int action, int currNodes,char *managementHost,
                int dependentJobId, pthread_t socketThread) {
    int world_size, world_rank, name_len;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(processor_name, &name_len);
    MPI_Barrier(MPI_COMM_WORLD);
    if (action == 2) {
        printf("(sergio)[%d]: %s(%s,%d) - proc: %s world: %d - current: %d\n", getpid(), __FILE__, __func__, __LINE__, processor_name, world_rank, currNodes);
        if (world_rank >= currNodes) {

            pid_t pid = fork();
            if (pid == -1) {
                fprintf(stderr, "Fork error\n");
                exit(-1);
            } else if (pid == 0) { //forked processes in the rest of the ranks are clients
                int sockfd;
                struct sockaddr_in serv_addr;
                struct hostent *server;
                sockfd = socket(AF_INET, SOCK_STREAM, 0);
                if (sockfd < 0) {
                    printf("ERROR opening socket");
                    exit(-1);
                }
                server = gethostbyname(managementHost); //managementHost);
                if (server == NULL) {
                    fprintf(stderr, "ERROR, no such host (%s)\n", managementHost);
                    exit(-1);
                }
                bzero((char *) &serv_addr, sizeof (serv_addr));
                //memset(&serv_addr, '\0', sizeof(serv_addr));
                serv_addr.sin_family = AF_INET;
                bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
                serv_addr.sin_port = htons(PORT);
                //printf("Before connecting from %s to %s - Socket: %d, size: %lu\n", processor_name, managementHost, sockfd, sizeof (serv_addr));
                while (getppid() != 1) {
                    //printf("\t(sergio): %s(%s,%d) - %s %d - pid %d, ppid %d\n", __FILE__, __func__, __LINE__, processor_name, world_rank, getpid(), getppid());
                    sleep(1);
                }
                //printf("PID: %d [%d/%d] PPID: %d - Before connecting From %s to %s (on socket %d - %lu len - (fam %i, addr %d, port %i)\n", getpid(), world_rank, world_size, getppid(), processor_name, DMR_managementHost, sockfd, sizeof (serv_addr), serv_addr.sin_family, serv_addr.sin_addr.s_addr, serv_addr.sin_port);
                int newconnect = connect(sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr));
                //printf("PID: %d [%d/%d] PPID: %d - After connecting (return %d) From %s to %s (on socket %d - %lu len - (fam %i, addr %d, port %i)\n", getpid(), world_rank, world_size, getppid(), newconnect, processor_name, DMR_managementHost, sockfd, sizeof (serv_addr), serv_addr.sin_family, serv_addr.sin_addr.s_addr, serv_addr.sin_port);
                if (newconnect < 0) {
                    fprintf(stderr, "PID: %d [%d/%d] ERROR connecting (return %d) From %s to %s (on socket %d - %lu len - (fam %i, addr %d, port %i)", getpid(), world_rank, world_size, newconnect, processor_name, managementHost, sockfd, sizeof (serv_addr), serv_addr.sin_family, serv_addr.sin_addr.s_addr, serv_addr.sin_port);
                    exit(-1);
                }
                //printf("After connecting from %s to %s - Socket: %d, size: %lu\n", processor_name, DMR_managementHost, sockfd, sizeof (serv_addr));
                close(sockfd);
                _exit(0);
            }
        } else if (world_rank == 0) {
            pthread_join(socketThread, NULL);
            job_desc_msg_t job_update;
            slurm_init_job_desc_msg(&job_update);
            job_update.job_id = dependentJobId;
            job_update.min_nodes = currNodes;
            printf("¿?¿?¿?¿?¿?[%d/%d] Deallocate last %d hosts (expand job %d, min_nodes %d)\n", world_rank, world_size, world_size - currNodes, job_update.job_id, job_update.min_nodes);
            sleep(1); //auxiliary wait for marenostrum (con 2 segundos funciona bien)
            slurm_update_job(&job_update);
            //printf("¿?¿?¿?¿?¿?[%d/%d] finish update\n", world_rank, world_size);
        }
    }
    printf("### [%d]-> %s(%s,%d) || Exiting... rank %d/%d from %s\n", getpid(), __func__, __FILE__, __LINE__, world_rank, world_size, processor_name);
    exit(0);

    //MPI_Finalize();
    //_exit(0);
    //return;

    //kill(getpid(), 9);
}

// MPI_Info wrapper class
// Avoids manual allocation/deallocation
// Enable transparent copies easily
// Can return a MPI_Info handle or be casted into it.
class HostInfo {
    MPI_Info info_;
public:
    HostInfo( MPI_Info info ) : info_(info)
    {
    }

    HostInfo( HostInfo const& o ) : info_() {
        int result = MPI_Info_dup( o.info_, &info_ );
        assert( result == MPI_SUCCESS );
    }

    HostInfo() : info_() {
        int result = MPI_Info_create( &info_ );
        assert( result == MPI_SUCCESS );
    }

    virtual ~HostInfo() {
        int result = MPI_Info_free( &info_ );
        assert( result == MPI_SUCCESS);
    }

    static HostInfo defaultSettings();

    // Cast operator. Should be const to allow
    // implicit vector copies to be performed
    // without errors.
    operator MPI_Info() const {
        return info_;
    }

    MPI_Info const& get() const {
        return info_;
    }

    MPI_Info& get() {
        return info_;
    }

    void set( const char* key, std::string const& value ) {

        if( !value.empty() )
            MPI_Info_set( info_, key, value.c_str() );
    }

    void set( const char* key, const char* value ) {
        MPI_Info_set( info_, key, value );
    }

    template<typename T>
    void set( const char* key, T const& value ) {
#ifdef HAVE_CXX11
        std::string svalue( std::to_string(value) );
#else
        std::stringstream ss;
        ss << value;
        std::string svalue( ss.str() );
#endif
        set( key, svalue );
    }
};

static inline void trim(std::string& params) {
    //Trim params
    size_t pos = params.find_last_not_of(" \t");
    if (std::string::npos != pos) params = params.substr(0, pos + 1);
    pos = params.find_first_not_of(" \t");
    if (std::string::npos != pos) params = params.substr(pos);
}

void callMPISpawn(
        MPI_Comm comm,
        const int availableHosts,
        const bool strict,
        std::vector<std::string>& tokensParams,
        std::vector<std::string>& tokensHost,
        std::vector<int>& hostInstances,
        const int* pph_list,
        const int process_per_host,
        const bool& shared,
        int& spawnedHosts,
        int& totalNumberOfSpawns,
        MPI_Comm* intercomm) {
    std::string mpiExecFile = "/home/bsc15/bsc15334/projects/lammps-malleable/src/lmp_malleable.INTEL64"; //nanos::ext::MPIProcessor::getMpiExecFile();
    std::string _mpiLauncherFile = "/home/bsc15/bsc15334/projects/lammps-malleable/src/lmp_malleable.INTEL64";// nanos::ext::MPIProcessor::getMpiLauncherFile();
    bool pphFromHostfile = process_per_host <= 0;
    bool usePPHList = pph_list != NULL;
    // Spawn the remote process using previously parsed parameters
    std::string result_str;
    if (!mpiExecFile.empty()) {
        result_str = mpiExecFile;
    }

    /** Build spawn structures */
    //Number of spawns = max length (one instance per host)
    std::vector<char **> argvs(availableHosts);
    std::vector<char *> commands(availableHosts, &_mpiLauncherFile[0]); // Warning: This may not work
    std::vector<HostInfo> host_info(availableHosts);
    std::vector<int> num_processes(availableHosts, 0);



    spawnedHosts = 0;
    //This the real length of previously declared arrays, it will be equal to number_of_spawns when
    //hostfile/line only has one instance per host (aka no host:nInstances)
    for (int hostCounter = 0; hostCounter < availableHosts; hostCounter++) {
        //Fill host
        std::string host;
        //Set number of instances this host can handle (depends if user specified, hostList specified or list specified)
        int currHostInstances;
        if (usePPHList) {
            currHostInstances = pph_list[hostCounter];
        } else if (pphFromHostfile) {
            currHostInstances = hostInstances.at(hostCounter);
        } else {
            currHostInstances = process_per_host;
        }
        if (currHostInstances != 0) {
            host = tokensHost.at(hostCounter);
            //If host is a file, give it to Intel, otherwise put the host in the spawn
            std::ifstream hostfile(host.c_str());
            bool isfile = hostfile.is_open();
            if (isfile) {
                std::string line;
                int number_of_lines_in_file = 0;
                while (std::getline(hostfile, line)) {
                    ++number_of_lines_in_file;
                }

                host_info[hostCounter].set("hostfile", host);
                currHostInstances = number_of_lines_in_file*currHostInstances;
            } else {
                host_info[hostCounter].set("host", host);
            }
            //In case the MPI implementation supports soft key...
            if (!strict) {
                host_info[hostCounter].set("soft", "0:N");
            }

            hostfile.close();

            //Fill parameter array (including env vars)
            std::stringstream allParamTmp(tokensParams.at(hostCounter));
            std::string tmpParam;
            int paramsSize = 3;
            while (getline(allParamTmp, tmpParam, ',')) {
                paramsSize++;
            }
            std::stringstream all_param(tokensParams.at(hostCounter));
            char **argvv = new char*[paramsSize];
            //Fill the params
            argvv[0] = const_cast<char*> (result_str.c_str());
            argvv[1] = const_cast<char*> ("empty");
            int paramCounter = 2;
            while (getline(all_param, tmpParam, ',')) {
                //Trim current param
                trim(tmpParam);
                char* arg_copy = new char[tmpParam.size() + 1];
                strcpy(arg_copy, tmpParam.c_str());
                argvv[paramCounter++] = arg_copy;
            }
            argvv[paramsSize - 1] = NULL;

            commands[spawnedHosts] = const_cast<char*> (_mpiLauncherFile.c_str());
            argvs[spawnedHosts] = argvv;
            num_processes[spawnedHosts] = currHostInstances;

            totalNumberOfSpawns += currHostInstances;
            ++spawnedHosts;
        }
    }
    //#ifndef OPEN_MPI
    //    int fd = -1;
    //    //std::string lockname=NANOX_PREFIX"/bin/nanox-pfm";
    //    std::string lockname = "./.ompssOffloadLock";
    //    while (!nanos::ext::MPIProcessor::isDisableSpawnLock() && !shared && fd == -1) {
    //        fd = tryGetLock(const_cast<char*> (lockname.c_str()));
    //    }
    //#endif
    std::vector<MPI_Info> array_of_mpiinfo(host_info.begin(), host_info.end());

    //printf("(sergio)[%d] SPAWN count %d %d %s %d\n", getpid(), spawnedHosts, availableHosts, commands.front(), num_processes.front());
    //int len = commands.size();
    //for (int i=0, i< len; i++)
    //    printf("(sergio)[%d][%d/%d] SPAWN count %d\n", spawnedHosts);
    //*array_of_commands[], char **array_of_argv[],
    //                        const int array_of_maxprocs[], const MPI_Info array_of_info[],
    //                        int root, MPI_Comm comm, MPI_Comm *intercomm, int array_of_errcodes[]%d\n", getpid(), world_rank, world_size, spawnedHosts,&commands.front(),&argvs.front(), &num_processes.front(),&array_of_mpiinfo.front(), 0, comm, intercomm, MPI_ERRCODES_IGNORE);

    MPI_Comm_spawn_multiple(spawnedHosts,
                            &commands.front(),
                            &argvs.front(), &num_processes.front(),
                            &array_of_mpiinfo.front(), 0, comm, intercomm, MPI_ERRCODES_IGNORE);
    //#ifndef OPEN_MPI
    //    if (!nanos::ext::MPIProcessor::isDisableSpawnLock() && !shared) {
    //        releaseLock(fd, const_cast<char*> (lockname.c_str()));
    //    }
    //#endif

    //Free all args sent
    for (int i = 0; i < spawnedHosts; i++) {
        //Free all args which were dynamically copied before
        for (int e = 2; argvs[i][e] != NULL; e++) {
            delete[] argvs[i][e];
        }
        delete[] argvs[i];
    }
}

void SLURMbuildHostLists(hostlist_t hl, std::vector<std::string>& tokensParams, std::vector<std::string>& tokensHost, std::vector<int>& hostInstances) {
    std::list<std::string> tmpStorage;
    std::string params = "ompssnoparam";

    char *host = slurm_hostlist_shift(hl);

    while (host != NULL) {
        //printf("[%d]-> %s(%s,%d) || host: %s\n", getpid(), __func__, __FILE__, __LINE__, host);
        //tmpStorage.push_back(line);
        hostInstances.push_back(1);
        tokensHost.push_back(host);
        tokensParams.push_back(params);

        host = slurm_hostlist_shift(hl);
    }
}




static void readableHostlist(char * nameExp, char * readableNames) {
    hostlist_t hl = slurm_hostlist_create(nameExp);
    char *host = slurm_hostlist_shift(hl);
    sprintf(readableNames, "%s", host);
    host = slurm_hostlist_shift(hl);
    while (host != NULL) {

        sprintf(readableNames, "%s,%s", readableNames, host);
        host = slurm_hostlist_shift(hl);
    }
    slurm_hostlist_destroy(hl);
}

//Return a comma-separated string of the first "counter" hosts.
static void readableHostlistTruncated(char * nameExp, char * readableNames, int counter) {
    int cnt = 0;
    hostlist_t hl = slurm_hostlist_create(nameExp);
    char *host = slurm_hostlist_shift(hl);
    sprintf(readableNames, "%s", host);
    host = slurm_hostlist_shift(hl);
    while (cnt < counter - 1 && host != NULL) {
        sprintf(readableNames, "%s,%s", readableNames, host);
        host = slurm_hostlist_shift(hl);
        cnt++;
    }
    slurm_hostlist_destroy(hl);
}


int DMRExpandJob(MPI_Comm comm, int number_of_hosts, int process_per_host,
                 MPI_Comm *intercomm, bool strict, int* provided, int offset, int* pph_list, int dependentJobId) {
    std::vector<std::string> tokensParams;
    std::vector<std::string> tokensHost;
    std::vector<int> hostInstances;
    int totalNumberOfSpawns = 0;
    int spawnedHosts = 0;
    int world_size, world_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    std::string tokensHostString; //, tokensParamsString;
    char *finalNodelist = (char *) malloc(sizeof(char) * STR_SIZE);

    _actionRMS = 0;

    job_desc_msg_t job_update;
    job_info_msg_t *MasterInfo, *ExpandInfo;
    char *jID = getenv("SLURM_JOBID");
    uint32_t jobID = atoi(jID);

    if (world_rank == 0) {
        slurm_load_job(&MasterInfo, jobID, 0);
        if (MasterInfo == NULL || MasterInfo->record_count == 0) {
            slurm_perror((char *) "%%%%%% No info %%%%%%");
            exit(1);
        }

        slurm_load_job(&ExpandInfo, dependentJobId, 0);
        if (ExpandInfo == NULL || ExpandInfo->record_count == 0) {
            slurm_perror((char *) "%%%%%% No info %%%%%%");
            exit(1);
        }

        char currHostnames[2048], appendHostnames[2048];
        readableHostlist(MasterInfo->job_array[0].nodes, currHostnames);
        readableHostlist(ExpandInfo->job_array[0].nodes, appendHostnames);
        sprintf(finalNodelist, "%s,%s", currHostnames, appendHostnames);
        //printf("[%d]-> %s(%s,%d) || %s - %s - %s\n", getpid(), __func__, __FILE__, __LINE__, currHostnames, appendHostnames, finalNodelist);
        slurm_free_job_info_msg(ExpandInfo);
        slurm_free_job_info_msg(MasterInfo);

        //$ scontrol update jobid=$SLURM_JOBID NumNodes=0
        slurm_init_job_desc_msg(&job_update);
        job_update.job_id = dependentJobId;
        job_update.min_nodes = 0;
        if (slurm_update_job(&job_update)) {
            slurm_perror((char *) "slurm_update_job error");
            exit(1);
        }
        //sleep(2);
        //exit
        if (slurm_kill_job(dependentJobId, SIGKILL, 0)) {
            slurm_perror((char *) "slurm_kill_job error");
            exit(1);
        }
        //sleep(2);
        //$ scontrol update jobid=$SLURM_JOBID NumNodes=ALL
        //printf("[%d]-> %s(%s,%d) || %d\n", getpid(), __func__, __FILE__, __LINE__, job.min_nodes);
        slurm_init_job_desc_msg(&job_update);
        job_update.job_id = jobID;
        job_update.min_nodes = INFINITE;
        if (slurm_update_job(&job_update)) {
            slurm_perror((char *) "slurm_update_job error");
            exit(1);
        }
        //sleep(2);
    }

    MPI_Bcast(finalNodelist, STR_SIZE + 1, MPI_CHAR, 0, comm);

    hostlist_t hl = slurm_hostlist_create(finalNodelist);
    SLURMbuildHostLists(hl, tokensParams, tokensHost, hostInstances);
    slurm_hostlist_destroy(hl);

    bool shared = (world_size > 1);
    //sleep(2);
    //printf("[%d/%d]:[%d]-> %s(%s,%d) || Nodes: %d (%s)\n", world_rank, world_size, getpid(), __func__, __FILE__, __LINE__, (int) hostInstances.size(), finalNodelist);
    free(finalNodelist);
    callMPISpawn(comm, hostInstances.size(), strict, tokensParams, tokensHost, hostInstances, pph_list,
                 process_per_host, shared, spawnedHosts, totalNumberOfSpawns, intercomm);

    if (provided != NULL) *provided = totalNumberOfSpawns;
    //    createNanoxStructures(comm, intercomm, spawnedHosts, totalNumberOfSpawns, shared, world_size, world_rank, pph_list);

    _actionRMS = 1; //> 0 indica que va a haber un spawn

    return 0;

}

int DMRShrinkJob(MPI_Comm comm, int number_of_hosts, int process_per_host,
                 MPI_Comm *intercomm, bool strict, int* provided, int offset, int* pph_list) {


    std::vector<std::string> tokensParams;
    std::vector<std::string> tokensHost;
    std::vector<int> hostInstances;
    int totalNumberOfSpawns = 0;
    int spawnedHosts = 0;
    int world_size, world_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    std::string tokensHostString; //, tokensParamsString;
    char *currHostnames = (char *) malloc(sizeof (char) * STR_SIZE);
    //char *finalNodelist = (char *) malloc(sizeof (char) * STR_SIZE);

    //printf("@@@[%d/%d]:[%d]-> %s(%s,%d)\n", rank, world_size, getpid(), __func__, __FILE__, __LINE__);
    _actionRMS = 0;

    job_info_msg_t *MasterInfo;
    char *pID = getenv("SLURM_JOBID");
    uint32_t procID = atoi(pID);

    if (world_rank == 0) {
        slurm_load_job(&MasterInfo, procID, 0);
        if (MasterInfo == NULL || MasterInfo->record_count == 0) {
            slurm_perror((char *) "%%%%%% No info %%%%%%");
            exit(1);
        }

        readableHostlistTruncated(MasterInfo->job_array[0].nodes, currHostnames, number_of_hosts);
        //printf("[%d]-> %s(%s,%d) || %s - %s\n", getpid(), __func__, __FILE__, __LINE__, currHostnames, MasterInfo->job_array[0].nodes);
        slurm_free_job_info_msg(MasterInfo);
    }

    MPI_Bcast(currHostnames, STR_SIZE + 1, MPI_CHAR, 0, comm);

    hostlist_t hl = slurm_hostlist_create(currHostnames);
    SLURMbuildHostLists(hl, tokensParams, tokensHost, hostInstances);
    slurm_hostlist_destroy(hl);

    //_currNodenames = (char *) malloc(sizeof (char) * strlen(currHostnames));
    //strcpy(_currNodenames, currHostnames);
    free(currHostnames);
    _currNodes = hostInstances.size();

    //Register spawned processes so nanox can use them
    int mpiSize;
    MPI_Comm_size(comm, &mpiSize);
    bool shared = (mpiSize > 1);
    //sleep(2);
    //printf("[%d/%d]:[%d]-> %s(%s,%d) || from %d nodes to %d (%s)\n", world_rank, world_size, getpid(), __func__, __FILE__, __LINE__, mpiSize, (int) hostInstances.size(), _currNodenames);
    callMPISpawn(comm, _currNodes, strict, tokensParams, tokensHost, hostInstances, pph_list,
                 process_per_host, shared, /* outputs*/ spawnedHosts, totalNumberOfSpawns, intercomm);

    if (provided != NULL) *provided = totalNumberOfSpawns;
//    createNanoxStructures(comm, intercomm, spawnedHosts, totalNumberOfSpawns, shared, mpiSize, world_rank, pph_list);

    _actionRMS = 2;

    return 0;
    //NANOS_MPI_CLOSE_IN_MPI_RUNTIME_EVENT;


}





/**
 * @brief Schedules the action through the Slurm policy and set up all the
 * necessary structures. This function also communicates with Nanox in order to
 * perform the action in the runtime.
 * @param min Minimum number of processes when shrinking.
 * @param max Maximum number of processes when expanding.
 * @param step Value used when resizing to a multiple of the current number of
 * processes.
 * @param pref Preferred number of processes running.
 * @param nnodes Number of nodes in the new communicator.
 * @param intercomm Communicator to spawn the new processes.
 * @param socketThread Identifier of the pthread waiting for socket connection.
 * This value is used to synchronize the processes in dmr_detach().Only used
 * when shrinking.
 * @param managementHost Name of the host from the operation is being managed.
 * Only used when shrinking.
 * @param dependentJobId Identifier of the dependet job. Only used when
 * shrinking.
 * @return Action identifier.
 * @see checkIterInhibitor
 */
int DMR_Reconfiguration(int min, int max, int step, int pref, int *nnodes,
                        MPI_Comm *intercomm, pthread_t *socketThread, char **managementHost, int *dependentJobId) {

    int world_size, world_rank, name_len;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(processor_name, &name_len);
    int counter = 0, action;

    /************************************************/
    bool check = false;
    if (world_rank == 0)
        check = checkInhibitors();
    //check = checkTimeInhibitor();
    MPI_Bcast(&check, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (!check)
        return -2;
    /************************************************/



    if (world_rank == 0) {
        char *pID = getenv("SLURM_JOBID");
        uint32_t procID = atoi(pID);
        *dependentJobId = (int) procID;
        job_info_msg_t *MasterInfo;
        slurm_load_job(&MasterInfo, procID, 0);
        *nnodes = 0;
        //Waiting for stabilisation version without sincronous sockets
        while (world_size != (int) MasterInfo->job_array->num_nodes) {
            printf("waiting for stabilization... (current %d - slurm %d)\n", world_size, MasterInfo->job_array->num_nodes);
            sleep(5);
            slurm_free_job_info_msg(MasterInfo);
            slurm_load_job(&MasterInfo, procID, 0);
        }


        //Slurm chooses the action in the selection policy
        _dmr_slurm_jobinfo_t *jobinfo = (_dmr_slurm_jobinfo_t *) malloc(sizeof (_dmr_slurm_jobinfo_t));
        MasterInfo->job_array->select_jobinfo->data = jobinfo;
        jobinfo->job_id = (uint32_t) procID;
        jobinfo->min = (uint32_t) min;
        jobinfo->max = (uint32_t) max;
        jobinfo->preference = (uint32_t) pref;
        jobinfo->step = (uint32_t) step;
        jobinfo->currentNodes = (uint32_t) world_size;

        slurm_get_select_jobinfo(MasterInfo->job_array->select_jobinfo, SELECT_JOBDATA_ACTION, NULL);

        action = jobinfo->action;
        *nnodes = jobinfo->resultantNodes;

        printf("[%d]-> %s(%s,%d) || Action: %d %d - Current nodes %d, while in slurm %d\n", getpid(), __func__, __FILE__, __LINE__, jobinfo->action, jobinfo->resultantNodes, world_size, MasterInfo->job_array->num_nodes);
        //free(jobinfo);

        if (action == 2) {
            *socketThread = (pthread_t) 0;
            *managementHost = (char *) malloc(sizeof (char) * MPI_MAX_PROCESSOR_NAME);
            strcpy(*managementHost, processor_name);
        }
        free(jobinfo);
        slurm_free_job_info_msg(MasterInfo);
    }



    if (world_rank == 0) {
        //printf("(sergio): %s(%s,%d) - action %d to end with %d nodes\n", __FILE__, __func__, __LINE__, _actionRMS, *nnodes);
        switch (action) {
        case 0:
        {
            break;
        }
        case 1:
        {//expand
            job_desc_msg_t job_desc;
            job_info_msg_t *jobAux;
            resource_allocation_response_msg_t* slurm_alloc_msg_ptr;
            slurm_init_job_desc_msg(&job_desc);
            job_desc.name = (char *) "resizer";
            job_desc.dependency = (char *) malloc(sizeof (char)*64);
            sprintf(job_desc.dependency, (char *) "expand:%d", *dependentJobId);
            job_desc.user_id = getuid();
            job_desc.priority = 1000000;
            job_desc.min_nodes = *nnodes - world_size;

            if (slurm_allocate_resources(&job_desc, &slurm_alloc_msg_ptr)) {
                slurm_perror((char *) "slurm_allocate_resources error (DMRCheckStatus)");
                exit(1);
            }
            free(job_desc.dependency);
            *dependentJobId = slurm_alloc_msg_ptr->job_id;
            if (slurm_alloc_msg_ptr->node_list == NULL) {
                slurm_load_job(&jobAux, *dependentJobId, 0);
                //printf("(sergio): %s(%s,%d) - %d %s\n", __FILE__, __func__, __LINE__, _expandJobId, jobAux->job_array->nodes);
                while (jobAux->job_array->nodes == NULL) {
                    slurm_free_job_info_msg(jobAux);
                    printf("Waiting for backfilling (check sinfo or priorities)\n");
                    counter++;
                    sleep(5);
                    if (counter == 10) {
                        printf("Backfilling not completed\n");
                        action = 0;
                        slurm_kill_job(*dependentJobId, SIGKILL, 0);
                        slurm_free_resource_allocation_response_msg(slurm_alloc_msg_ptr);
                        *dependentJobId = -1;
                        break;
                    }
                    slurm_load_job(&jobAux, *dependentJobId, 0);
                }
                if (action != 0)
                    slurm_free_job_info_msg(jobAux);
            }
            break;
        }
        case 2:
        {//shrink
            int n = world_size - *nnodes;
            printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
            //std::cout << "before creating &DMR_socketThread=" << DMR_socketThread << " addr=" << &DMR_socketThread << "\n";
            if (pthread_create(socketThread, NULL, listener, (void *) &n) < 0) {
                fprintf(stderr, "could not create thread");
                exit(-1);
            }
            //std::cout << "created &DMR_socketThread=" << DMR_socketThread << " addr=" << &DMR_socketThread << "\n";
            break;
        }
        }
    }
    //printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
    MPI_Bcast(&action, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(nnodes, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (action == 2) {
        if (world_rank > 0)
            *managementHost = (char *) malloc(sizeof (char) * MPI_MAX_PROCESSOR_NAME);
        MPI_Bcast(*managementHost, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, MPI_COMM_WORLD);
    }

    switch (action) {
    case 0:
    {
        break;
    }
    case 1:
    {//expand
        //        nanosExpandJob(MPI_COMM_WORLD, *nnodes, 1, intercomm, true, NULL, 0, NULL, *dependentJobId);
        DMRExpandJob(MPI_COMM_WORLD, *nnodes, 1, intercomm, true, NULL, 0, NULL, *dependentJobId);
        break;
    }
    case 2:
    {//shrink
        //        nanosShrinkJob(MPI_COMM_WORLD, *nnodes, 1, intercomm, true, NULL, 0, NULL);
        DMRShrinkJob(MPI_COMM_WORLD, *nnodes, 1, intercomm, true, NULL, 0, NULL);
        break;
    }
    }
    //printf("[%d/%d](sergio): %s(%s,%d) %lu %d\n", world_rank, world_size, __FILE__, __func__, __LINE__, (unsigned long int) *socketThread, *nnodes);
    return action;
}






/*
void dmr_comm_disconnect(MPI_Comm comm) {
    int comm_size, parent_comm_size, factor, dst, i, my_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(comm, &parent_comm_size);

    if (parent_comm_size > comm_size) { //shrink
        factor = parent_comm_size / comm_size;
        for (i = 0; i < factor; i++) {
            dst = my_rank * factor + i;
            MPI_Send(&dst, 1, MPI_INT, dst, 1205, comm);
            //res = nanosMPISend(buf, count, datatype, dst, 1205, comm);
        }
    }// else { //expand
     //   nanosMPISend(buf, count, datatype, nanos::ext::MPIRemoteNode::getCurrentTaskParent(), TAG_END_TASK, comm);
    //}
    MPI_Comm_disconnect(&comm);\
}
 */

/**
 * @brief Initializes reconfiguration parameters in order to take appropriate
 * actions and leave ready the internal structures. This function MUST be called
 *  before using the DMR_RECONFIG macro.
 * @param min Minimum number of processes when shrinking.
 * @param max Maximum number of processes when expanding.
 * @param pref Preferred number of processes running.
 */
void DMR_Set_parameters(int min, int max, int pref) {
    DMR_min = min;
    DMR_max = max;
    DMR_pref = pref;
    DMR_refTime = getWalltime();
}
