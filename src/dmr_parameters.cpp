#include "dmr_parameters.h"


#include <mpi.h>
#include "force.h"
#include "lammps.h"


using namespace LAMMPS_NS;

DMR::DMR(LAMMPS *lmp) : Pointers(lmp) {

}

void DMR::command(int narg, char **arg) {
    lmp->min_procs = force->bnumeric(FLERR, arg[0]);
    lmp->max_procs = force->bnumeric(FLERR, arg[1]);
    lmp->pref_procs = force->bnumeric(FLERR, arg[2]);
}
