/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */
#include <iostream>
#include <string.h>
#include <fstream>
#include "verlet_dmr.h"
#include "neighbor.h"
#include "domain.h"
#include "comm.h"
#include "atom.h"
#include "atom_vec.h"
#include "force.h"
#include "pair.h"
#include "bond.h"
#include "angle.h"
#include "dihedral.h"
#include "improper.h"
#include "kspace.h"
#include "output.h"
#include "update.h"
#include "modify.h"
#include "compute.h"
#include "fix.h"
#include "timer.h"
#include "memory.h"
#include "error.h"
#include "input.h"
#include "dmr.h"
#include "write_restart.h"
#include "read_restart.h"
#include "dmr_lammps.h"
using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

Verlet_DMR::Verlet_DMR(LAMMPS *lmp, int narg, char **arg) :
    Integrate(lmp, narg, arg) {}

/* ----------------------------------------------------------------------
   initialization before run
------------------------------------------------------------------------- */

void Verlet_DMR::init()
{
    Integrate::init();

    // warn if no fixes

    if (modify->nfix == 0 && comm->me == 0)
        error->warning(FLERR,"No fixes defined, atoms won't move");

    // virial_style:
    // 1 if computed explicitly by pair->compute via sum over pair interactions
    // 2 if computed implicitly by pair->virial_fdotr_compute via sum over ghosts

    if (force->newton_pair) virial_style = 2;
    else virial_style = 1;

    // setup lists of computes for global and per-atom PE and pressure

    ev_setup();

    // detect if fix omp is present for clearing force arrays

    int ifix = modify->find_fix("package_omp");
    if (ifix >= 0) external_force_clear = 1;

    // set flags for arrays to clear in force_clear()

    torqueflag = extraflag = 0;
    if (atom->torque_flag) torqueflag = 1;
    if (atom->avec->forceclearflag) extraflag = 1;

    // orthogonal vs triclinic simulation box

    triclinic = domain->triclinic;
}

/* ----------------------------------------------------------------------
   setup before run
------------------------------------------------------------------------- */

void Verlet_DMR::setup(int flag)
{
    if (comm->me == 0 && screen) {
        fprintf(screen,"Setting up Verlet run ...\n");
        if (flag) {
            fprintf(screen,"  Unit style    : %s\n",update->unit_style);
            fprintf(screen,"  Current step  : " BIGINT_FORMAT "\n",update->ntimestep);
            fprintf(screen,"  Time step     : %g\n",update->dt);
            timer->print_timeout(screen);
        }
    }

    if (lmp->kokkos)
        error->all(FLERR,"KOKKOS package requires run_style verlet/kk");

    update->setupflag = 1;

    // setup domain, communication and neighboring
    // acquire ghosts
    // build neighbor lists

    atom->setup();
    modify->setup_pre_exchange();
    if (triclinic) domain->x2lamda(atom->nlocal);
    domain->pbc();
    domain->reset_box();
    comm->setup();
    if (neighbor->style) neighbor->setup_bins();
    comm->exchange();
    if (atom->sortfreq > 0) atom->sort();
    comm->borders();
    if (triclinic) domain->lamda2x(atom->nlocal+atom->nghost);
    domain->image_check();
    domain->box_too_small_check();
    modify->setup_pre_neighbor();
    neighbor->build(1);
    modify->setup_post_neighbor();
    neighbor->ncalls = 0;

    // compute all forces

    force->setup();
    ev_set(update->ntimestep);
    force_clear();
    modify->setup_pre_force(vflag);

    if (pair_compute_flag) force->pair->compute(eflag,vflag);
    else if (force->pair) force->pair->compute_dummy(eflag,vflag);

    if (atom->molecular) {
        if (force->bond) force->bond->compute(eflag,vflag);
        if (force->angle) force->angle->compute(eflag,vflag);
        if (force->dihedral) force->dihedral->compute(eflag,vflag);
        if (force->improper) force->improper->compute(eflag,vflag);
    }

    if (force->kspace) {
        force->kspace->setup();
        if (kspace_compute_flag) force->kspace->compute(eflag,vflag);
        else force->kspace->compute_dummy(eflag,vflag);
    }

    modify->setup_pre_reverse(eflag,vflag);
    if (force->newton) comm->reverse_comm();

    modify->setup(vflag);
    output->setup(flag);
    update->setupflag = 0;
}

/* ----------------------------------------------------------------------
   setup without output
   flag = 0 = just force calculation
   flag = 1 = reneighbor and force calculation
------------------------------------------------------------------------- */

void Verlet_DMR::setup_minimal(int flag)
{
    update->setupflag = 1;

    // setup domain, communication and neighboring
    // acquire ghosts
    // build neighbor lists

    if (flag) {
        modify->setup_pre_exchange();
        if (triclinic) domain->x2lamda(atom->nlocal);
        domain->pbc();
        domain->reset_box();
        comm->setup();
        if (neighbor->style) neighbor->setup_bins();
        comm->exchange();
        comm->borders();
        if (triclinic) domain->lamda2x(atom->nlocal+atom->nghost);
        domain->image_check();
        domain->box_too_small_check();
        modify->setup_pre_neighbor();
        neighbor->build(1);
        modify->setup_post_neighbor();
        neighbor->ncalls = 0;
    }

    // compute all forces

    ev_set(update->ntimestep);
    force_clear();
    modify->setup_pre_force(vflag);

    if (pair_compute_flag) force->pair->compute(eflag,vflag);
    else if (force->pair) force->pair->compute_dummy(eflag,vflag);

    if (atom->molecular) {
        if (force->bond) force->bond->compute(eflag,vflag);
        if (force->angle) force->angle->compute(eflag,vflag);
        if (force->dihedral) force->dihedral->compute(eflag,vflag);
        if (force->improper) force->improper->compute(eflag,vflag);
    }

    if (force->kspace) {
        force->kspace->setup();
        if (kspace_compute_flag) force->kspace->compute(eflag,vflag);
        else force->kspace->compute_dummy(eflag,vflag);
    }

    modify->setup_pre_reverse(eflag,vflag);
    if (force->newton) comm->reverse_comm();

    modify->setup(vflag);
    update->setupflag = 0;
}

/* ----------------------------------------------------------------------
   run for N steps
------------------------------------------------------------------------- */
/**
 * @brief Verlet_DMR::sendEx
 * this function is not being used in malleable lammps
 * its only used if malleable lammps needs to be run using checkpoint/restart mechanism
 */
void Verlet_DMR::sendEx() {

    int rank, world_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    double time2;
    if(rank == 0)
        time2 = MPI_Wtime();


    if(rank == 0) {
        MPI_Send(&lmp->input_argc, 1, MPI_INT, 0, 100, DMR_INTERCOMM);
        for(int i = 0; i < lmp->input_argc; ++i)
            MPI_Send(lmp->input_argv[i], 50, MPI_CHAR, 0, 100, DMR_INTERCOMM);
    }

    lmp->init();
    if (domain->triclinic) domain->x2lamda(atom->nlocal);
    domain->pbc();
    domain->reset_box();
    comm->setup();
    comm->exchange();
    comm->borders();
    if (domain->triclinic) domain->lamda2x(atom->nlocal+atom->nghost);

    WriteRestart *restart_dmr = new WriteRestart(lmp);
    restart_dmr->multiproc_options(0, 0, 0, NULL);

    restart_dmr->write("dmr_restart.dat");
    MPI_Barrier(DMR_INTERCOMM);


}

/**
 * @brief Verlet_DMR::recvEx
 * redundant function
 */
void Verlet_DMR::recvEx() {}

void Verlet_DMR::sendSh() {}

void Verlet_DMR::recvSh() {}

/**
 * @brief run_new
 * this function is run in the new processes created by DMRLIB
 * only used if lammps is run with checkpoint/restart malleability
 */
void run_new() {
    int me;
    int remote_size, local_size;
    MPI_Comm DMR_INTERCOMM;
    MPI_Comm_rank(MPI_COMM_WORLD, &me);
    MPI_Comm_size(MPI_COMM_WORLD, &local_size);
    MPI_Comm_get_parent(&DMR_INTERCOMM);
    MPI_Comm_remote_size(DMR_INTERCOMM, &remote_size);

    int input_argc;
    char **input_argv;

    if(me == 0) {
        MPI_Recv(&input_argc, 1, MPI_INT, 0, 100, DMR_INTERCOMM, MPI_STATUS_IGNORE);
    }
    MPI_Bcast(&input_argc, 1, MPI_INT, 0, MPI_COMM_WORLD);
    input_argv = (char **) new char*[input_argc];

    for(int i = 0; i < input_argc; ++i) {
        input_argv[i] = (char*) new char*[50];
        if(me == 0)
            MPI_Recv(input_argv[i], 50, MPI_CHAR, 0, 100, DMR_INTERCOMM, MPI_STATUS_IGNORE);
        MPI_Bcast(input_argv[i], 50, MPI_CHAR, 0, MPI_COMM_WORLD);

    }

    LAMMPS *lammps = new LAMMPS(input_argc, input_argv, MPI_COMM_WORLD);
    lammps->input->dmr_restart_flag = 1;

    ReadRestart *read_restart = new ReadRestart(lammps);
    char *arg[] = {"dmr_restart.dat"};
    MPI_Barrier(DMR_INTERCOMM);
    read_restart->command(1, arg);
    lammps->input->file();

    delete lammps;
    MPI_Finalize();
}

void Verlet_DMR::run(int n) {
    DMR_Set_parameters(lmp->min_procs, lmp->max_procs, lmp->pref_procs);

    bigint ntimestep;
    int nflag,sortflag;

    int n_post_integrate = modify->n_post_integrate;
    int n_pre_exchange = modify->n_pre_exchange;
    int n_pre_neighbor = modify->n_pre_neighbor;
    int n_post_neighbor = modify->n_post_neighbor;
    int n_pre_force = modify->n_pre_force;
    int n_pre_reverse = modify->n_pre_reverse;
    int n_post_force = modify->n_post_force;
    int n_end_of_step = modify->n_end_of_step;

    if (atom->sortfreq > 0) sortflag = 1;
    else sortflag = 0;

    int me;
    MPI_Comm_rank(MPI_COMM_WORLD, &me);
    if(me == 0)
        std::cout << "Number of Iterations left: " << n << std::endl;

    DMR_LAMMPS *dmr_lammps = new DMR_LAMMPS(lmp, DMR_INTERCOMM);

    double time_inititalization;
    if(comm->me == 0) {
        time_inititalization = MPI_Wtime() - lmp->walltime;
        std::cout << "Initialization time: " << time_inititalization << std::endl;
    }

    std::fstream fs;
    if(me == 0) {
        fs.open("data.txt", std::fstream::in | std::fstream::out | std::fstream::app);
        fs << time_inititalization << " ";
        fs.close();
    }


    double time1, time_reconfiguration_check, time_iteration;

    for (int i = 0; i < n; i++) {
        if(me == 0)
            time1 =  MPI_Wtime();

        dmr_lammps->walltime = time1;
        // for checkpoint/restart malleability
     //          DMR_RECONFIG(initialize_new_cr(),
   //                        dmr_lammps->send_expand_cr(),
 //                        dmr_lammps->recv_expand_cr(),
  //                     dmr_lammps->send_expand_cr(),
//                    dmr_lammps->recv_shrink_cr());

        // for MPI_Send/Receive malleability
        DMR_RECONFIG(initialize_new(),
                     dmr_lammps->send_expand(),
                     dmr_lammps->recv_expand(),
                     dmr_lammps->send_shrink(),
                     dmr_lammps->recv_shrink());


        if(me == 0) {
            time_reconfiguration_check = MPI_Wtime() - time1;
            std::cout << "Reconfiguration check time: " << time_reconfiguration_check << std::endl;
            time1 = MPI_Wtime();
        }

        if (timer->check_timeout(i)) {
            update->nsteps = i;
            break;
        }

        ntimestep = ++update->ntimestep;
        ev_set(ntimestep);

        // initial time integration

        timer->stamp();
        modify->initial_integrate(vflag);
        if (n_post_integrate) modify->post_integrate();
        timer->stamp(Timer::MODIFY);

        // regular communication vs neighbor list rebuild
        nflag = neighbor->decide();


        if (nflag == 0) {
            timer->stamp();
            comm->forward_comm();
            timer->stamp(Timer::COMM);
        } else {
            if (n_pre_exchange) {
                timer->stamp();
                modify->pre_exchange();
                timer->stamp(Timer::MODIFY);
            }
            if (triclinic) domain->x2lamda(atom->nlocal);
            domain->pbc();
            if (domain->box_change) {
                domain->reset_box();
                comm->setup();
                if (neighbor->style) neighbor->setup_bins();
            }
            timer->stamp();
            comm->exchange();
            if (sortflag && ntimestep >= atom->nextsort) atom->sort();
            comm->borders();
            if (triclinic) domain->lamda2x(atom->nlocal+atom->nghost);
            timer->stamp(Timer::COMM);
            if (n_pre_neighbor) {
                modify->pre_neighbor();
                timer->stamp(Timer::MODIFY);
            }
            neighbor->build(1);
            timer->stamp(Timer::NEIGH);
            if (n_post_neighbor) {
                modify->post_neighbor();
                timer->stamp(Timer::MODIFY);
            }
        }

        // force computations
        // important for pair to come before bonded contributions
        // since some bonded potentials tally pairwise energy/virial
        // and Pair:ev_tally() needs to be called before any tallying

        force_clear();

        timer->stamp();

        if (n_pre_force) {
            modify->pre_force(vflag);
            timer->stamp(Timer::MODIFY);
        }

        if (pair_compute_flag) {
            force->pair->compute(eflag,vflag);
            timer->stamp(Timer::PAIR);
        }

        if (atom->molecular) {
            if (force->bond) force->bond->compute(eflag,vflag);
            if (force->angle) force->angle->compute(eflag,vflag);
            if (force->dihedral) force->dihedral->compute(eflag,vflag);
            if (force->improper) force->improper->compute(eflag,vflag);
            timer->stamp(Timer::BOND);
        }

        if (kspace_compute_flag) {
            force->kspace->compute(eflag,vflag);
            timer->stamp(Timer::KSPACE);
        }

        if (n_pre_reverse) {
            modify->pre_reverse(eflag,vflag);
            timer->stamp(Timer::MODIFY);
        }

        // reverse communication of forces

        if (force->newton) {
            comm->reverse_comm();
            timer->stamp(Timer::COMM);
        }

        // force modifications, final time integration, diagnostics

        if (n_post_force) modify->post_force(vflag);
        modify->final_integrate();
        if (n_end_of_step) modify->end_of_step();
        timer->stamp(Timer::MODIFY);

        // all output

        if (ntimestep == output->next) {
            timer->stamp();
            output->write(ntimestep);
            timer->stamp(Timer::OUTPUT);
        }
        if(me == 0) {
            time_iteration = MPI_Wtime() - time1;
            std::cout << "Iteration time: " << time_iteration << std::endl;
        }
    }



    if(me == 0) {
        fs.open("data.txt", std::fstream::in | std::fstream::out | std::fstream::app);
        fs << time_reconfiguration_check << " " << time_iteration << std::endl;
        fs.close();
    }
}


/* ---------------------------------------------------------------------- */

void Verlet_DMR::cleanup()
{
    modify->post_run();
    domain->box_too_small_check();
    update->update_time();
}

/* ----------------------------------------------------------------------
   clear force on own & ghost atoms
   clear other arrays as needed
------------------------------------------------------------------------- */

void Verlet_DMR::force_clear()
{
    size_t nbytes;

    if (external_force_clear) return;

    // clear force on all particles
    // if either newton flag is set, also include ghosts
    // when using threads always clear all forces.

    int nlocal = atom->nlocal;

    if (neighbor->includegroup == 0) {
        nbytes = sizeof(double) * nlocal;
        if (force->newton) nbytes += sizeof(double) * atom->nghost;

        if (nbytes) {
            memset(&atom->f[0][0],0,3*nbytes);
            if (torqueflag) memset(&atom->torque[0][0],0,3*nbytes);
            if (extraflag) atom->avec->force_clear(0,nbytes);
        }

        // neighbor includegroup flag is set
        // clear force only on initial nfirst particles
        // if either newton flag is set, also include ghosts

    } else {
        nbytes = sizeof(double) * atom->nfirst;

        if (nbytes) {
            memset(&atom->f[0][0],0,3*nbytes);
            if (torqueflag) memset(&atom->torque[0][0],0,3*nbytes);
            if (extraflag) atom->avec->force_clear(0,nbytes);
        }

        if (force->newton) {
            nbytes = sizeof(double) * atom->nghost;

            if (nbytes) {
                memset(&atom->f[nlocal][0],0,3*nbytes);
                if (torqueflag) memset(&atom->torque[nlocal][0],0,3*nbytes);
                if (extraflag) atom->avec->force_clear(nlocal,nbytes);
            }
        }
    }
}
