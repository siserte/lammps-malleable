#ifdef COMMAND_CLASS

CommandStyle(dmr_parameters,DMR)

#else


#ifndef DMR_PARAMETERS_H
#define DMR_PARAMETERS_H

#include "pointers.h"

namespace LAMMPS_NS {

class DMR : protected Pointers {
public:
    DMR(LAMMPS *);
    void command(int, char **);
};
}

#endif // DMR_PARAMETERS_H
#endif
