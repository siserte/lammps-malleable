#!/bin/bash
#SBATCH --qos=debug
##SBATCH --partition=bsc_cs
#SBATCH --time=00:10:00
##SBATCH --nodes=999999999
#SBATCH --exclusive

#rm slurm-dmr.*
#echo $OMP_NUM_THREADS

SLURM_BIN=$HOME/apps/install/slurm/bin
SLURM_SBIN=$HOME/apps/install/slurm/sbin
SLURM_CONF_DIR=$HOME/slurm-confdir
SLURM_CONF_FILE=$SLURM_CONF_DIR/slurm.conf

i=0;
nodes=""
coma=","
cp $SLURM_CONF_DIR/slurm.conf.base $SLURM_CONF_FILE
rm -rf $HOME/apps/install/slurm/var/slurm*
rm hostfile.txt

echo "" >  $HOME/apps/install/slurm/var/accounting

NODELIST="$(scontrol show hostname $SLURM_JOB_NODELIST | paste -d -s)"
for node in $NODELIST; do
	#echo "$node - $(hostname)"
	if [ "$node" == "$(hostname)" ]; then
		echo "ControlMachine=$(hostname)" >> $SLURM_CONF_FILE
		#echo "Controller $(hostname)"
	else
		echo $node >> hostfile.txt
		echo "NodeName=$node CPUs=48 CoresPerSocket=24 ThreadsPerCore=1 State=Idle Port=7009" >> $SLURM_CONF_FILE
		nodes=$node$coma$nodes
	fi
done;

echo "PartitionName=dmrTest Nodes=$(echo $nodes | sed 's/.$//') Default=YES MaxTime=INFINITE State=UP" >> $SLURM_CONF_FILE

NNODES=$(($SLURM_NNODES-1))
NODELIST="$(scontrol show hostname $SLURM_JOB_NODELIST | paste -d, -s)"
#mpiexec -n $NNODES --hosts=$NODELIST hostname
$SLURM_SBIN/slurmctld -cDv  &
mpiexec -n $NNODES --hostfile hostfile.txt $SLURM_SBIN/slurmd -cDv &
$SLURM_BIN/sinfo


#$SLURM_BIN/sbatch -Jscal$NNODES -N$NNODES ./jobOf.sh &
$SLURM_BIN/sbatch -Jscal$NNODES -N2 ./launch.sh &

$SLURM_BIN/squeue
$SLURM_BIN/sinfo

aux=$( $SLURM_BIN/squeue | wc -l );
while [ $aux -gt 1 ]; do
	aux=$( $SLURM_BIN/squeue | wc -l );
	echo "$aux jobs remaining...";
	sleep 10;
done

echo "Finishing...";
#$SLURM_BIN/sacct

#sbatch mniv_submission.sh
