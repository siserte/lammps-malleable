#include <cstdio>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include "pointers.h"
#include "dmr_lammps.h"
#include "neighbor.h"
#include "domain.h"
#include "comm.h"
#include "atom.h"
#include "atom_vec.h"
#include "force.h"
#include "pair.h"
#include "bond.h"
#include "angle.h"
#include "dihedral.h"
#include "improper.h"
#include "kspace.h"
#include "group.h"
#include "output.h"
#include "update.h"
#include "modify.h"
#include "compute.h"
#include "fix.h"
#include "timer.h"
#include "memory.h"
#include "error.h"
#include "input.h"
#include "universe.h"
#include "write_restart.h"
#include "read_restart.h"
#include "irregular.h"

using namespace LAMMPS_NS;

#define MAX_GROUP 32
#define EPSILON 1.0e-6

enum{VERSION,SMALLINT,TAGINT,BIGINT,
     UNITS,NTIMESTEP,DIMENSION,NPROCS,PROCGRID,
     NEWTON_PAIR,NEWTON_BOND,
     XPERIODIC,YPERIODIC,ZPERIODIC,BOUNDARY,
     ATOM_STYLE,NATOMS,NTYPES,
     NBONDS,NBONDTYPES,BOND_PER_ATOM,
     NANGLES,NANGLETYPES,ANGLE_PER_ATOM,
     NDIHEDRALS,NDIHEDRALTYPES,DIHEDRAL_PER_ATOM,
     NIMPROPERS,NIMPROPERTYPES,IMPROPER_PER_ATOM,
     TRICLINIC,BOXLO,BOXHI,XY,XZ,YZ,
     SPECIAL_LJ,SPECIAL_COUL,
     MASS,PAIR,BOND,ANGLE,DIHEDRAL,IMPROPER,
     MULTIPROC,MPIIO,PROCSPERFILE,PERPROC,
     IMAGEINT,BOUNDMIN,TIMESTEP,
     ATOM_ID,ATOM_MAP_STYLE,ATOM_MAP_USER,ATOM_SORTFREQ,ATOM_SORTBIN,
     COMM_MODE,COMM_CUTOFF,COMM_VEL,NO_PAIR};

DMR_LAMMPS::DMR_LAMMPS(LAMMPS *lmp, MPI_Comm &comm) : Pointers(lmp), DMR_INTERCOMM(comm), walltime(0.) {
    MPI_Comm_rank(world, &me);
    MPI_Comm_size(world, &nprocs);
    buf.clear();
    pos = 0;
    min_procs = 1;
    max_procs = 16;
    pref_procs = 16;
}

void DMR_LAMMPS::send_expand() {
    double time2;
    if(me == 0)
        time2 = MPI_Wtime();



    if(me == 0) {
        MPI_Send(&lmp->input_argc, 1, MPI_INT, 0, 100, DMR_INTERCOMM);
        for(int i = 0; i < lmp->input_argc; ++i)
            MPI_Send(lmp->input_argv[i], 50, MPI_CHAR, 0, 100, DMR_INTERCOMM);
    }


    // getting ready to send atoms to new processes
    lmp->init();
    if (domain->triclinic) domain->x2lamda(atom->nlocal);
    domain->pbc();
    domain->reset_box();
    comm->setup();
    comm->exchange();
    comm->borders();
    if (domain->triclinic) domain->lamda2x(atom->nlocal+atom->nghost);


    WriteRestart *restart_dmr = new WriteRestart(lmp);
    char *buffer = new char[1000];
    restart_dmr->write_dmr(buffer);

    if(me == 0) {
        int n = strlen(buffer) + 1;

        MPI_Send(&n, 1, MPI_INT, 0, 0, DMR_INTERCOMM);
        MPI_Send(buffer, 1000, MPI_CHAR, 0, 1, DMR_INTERCOMM);
    }

    // send the atoms
    atom->avec->send_expand(DMR_INTERCOMM);

    //    MPI_Barrier(DMR_INTERCOMM);

    if(me == 0)
        std::cout << "Send time: " << MPI_Wtime() - time2 << std::endl;

    if(me == 0)
        MPI_Send(&walltime, 1, MPI_DOUBLE, 0, 55, DMR_INTERCOMM);
}

void DMR_LAMMPS::recv_expand() {

}

void DMR_LAMMPS::send_shrink() {

    double time2;
    if(me == 0)
        time2 = MPI_Wtime();



    if(me == 0) {
        MPI_Send(&lmp->input_argc, 1, MPI_INT, 0, 100, DMR_INTERCOMM);
        for(int i = 0; i < lmp->input_argc; ++i)
            MPI_Send(lmp->input_argv[i], 50, MPI_CHAR, 0, 100, DMR_INTERCOMM);
    }



    // getting ready to send atoms to new processes
    lmp->init();
    if (domain->triclinic) domain->x2lamda(atom->nlocal);
    domain->pbc();
    domain->reset_box();
    comm->setup();
    comm->exchange();
    comm->borders();
    if (domain->triclinic) domain->lamda2x(atom->nlocal+atom->nghost);


    WriteRestart *restart_dmr = new WriteRestart(lmp);
    char *buffer = new char[1000];
    restart_dmr->write_dmr(buffer);

    if(me == 0) {
        int n = strlen(buffer) + 1;
        MPI_Send(&n, 1, MPI_INT, 0, 0, DMR_INTERCOMM);
        MPI_Send(buffer, 1000, MPI_CHAR, 0, 1, DMR_INTERCOMM);
    }

    // send the atoms
    atom->avec->send_shrink(DMR_INTERCOMM);

    //    MPI_Barrier(DMR_INTERCOMM);

    if(me == 0)
        std::cout << "Send time: " << MPI_Wtime() - time2 << std::endl;

    if(me == 0)
        MPI_Send(&walltime, 1, MPI_DOUBLE, 0, 55, DMR_INTERCOMM);

}

void DMR_LAMMPS::recv_shrink()
{

}

DMR_LAMMPS::~DMR_LAMMPS()
{

}

void initialize_new() {

    
    double time3;
    int me;
    int remote_size, local_size;
    MPI_Comm DMR_INTERCOMM;
    MPI_Comm_rank(MPI_COMM_WORLD, &me);
    MPI_Comm_size(MPI_COMM_WORLD, &local_size);
    MPI_Comm_get_parent(&DMR_INTERCOMM);
    MPI_Comm_remote_size(DMR_INTERCOMM, &remote_size);

    if(me == 0)
        time3 = MPI_Wtime();

    int input_argc;
    char **input_argv;

    if(me == 0) {
        MPI_Recv(&input_argc, 1, MPI_INT, 0, 100, DMR_INTERCOMM, MPI_STATUS_IGNORE);


    }
    MPI_Bcast(&input_argc, 1, MPI_INT, 0, MPI_COMM_WORLD);
    input_argv = (char **) new char*[input_argc];

    for(int i = 0; i < input_argc; ++i) {
        input_argv[i] = (char*) new char*[50];
        if(me == 0)
            MPI_Recv(input_argv[i], 50, MPI_CHAR, 0, 100, DMR_INTERCOMM, MPI_STATUS_IGNORE);
        MPI_Bcast(input_argv[i], 50, MPI_CHAR, 0, MPI_COMM_WORLD);

    }





//    int argc = 3;
//    char *argv[3] = {"lmp_malleable", "-in", "./in.lj"};

LAMMPS *lammps = new LAMMPS(input_argc, input_argv, MPI_COMM_WORLD);
lammps->input->dmr_restart_flag = 1;






int n;
if(me == 0)
MPI_Recv(&n, 1, MPI_INT, 0, 0, DMR_INTERCOMM, MPI_STATUS_IGNORE);

char *buffer;
if(me == 0) {
    buffer = new char[1000];
    MPI_Recv(buffer, 1000, MPI_CHAR, 0, 1, DMR_INTERCOMM, MPI_STATUS_IGNORE);
}


ReadRestart *restart_dmr = new ReadRestart(lammps);
int nbuf = restart_dmr->read_dmr(buffer);


if(local_size > remote_size)
lammps->atom->avec->recv_expand(DMR_INTERCOMM);
else if(local_size < remote_size)
lammps->atom->avec->recv_shrink(DMR_INTERCOMM);
else
lammps->error->all(FLERR, "Reconfiguration without resizing!");





// if remapflag set, remap all atoms I read back to box before migrating
int remapflag = 1;
if (remapflag) {
    double **x = lammps->atom->x;
    imageint *image = lammps->atom->image;
    int nlocal = lammps->atom->nlocal;

    for (int i = 0; i < nlocal; i++)
        lammps->domain->remap(x[i],image[i]);
}

// create a temporary fix to hold and migrate extra atom info
// necessary b/c irregular will migrate atoms

//    if (nextra) {
//        char cextra[8],fixextra[8];
//        sprintf(cextra,"%d",nextra);
//        sprintf(fixextra,"%d",modify->nfix_restart_peratom);
//        char **newarg = new char*[5];
//        newarg[0] = (char *) "_read_restart";
//        newarg[1] = (char *) "all";
//        newarg[2] = (char *) "READ_RESTART";
//        newarg[3] = cextra;
//        newarg[4] = fixextra;
//        modify->add_fix(5,newarg);
//        delete [] newarg;
//    }

// move atoms to new processors via irregular()
// turn sorting on in migrate_atoms() to avoid non-reproducible restarts
// in case read by different proc than wrote restart file
// first do map_init() since irregular->migrate_atoms() will do map_clear()

if (lammps->atom->map_style) {
    lammps->atom->map_init();
    lammps->atom->map_set();
}
if(lammps->domain->triclinic) lammps->domain->x2lamda(lammps->atom->nlocal);
Irregular *irregular = new Irregular(lammps);
irregular->migrate_atoms(1);
delete irregular;
if(lammps->domain->triclinic) lammps->domain->lamda2x(lammps->atom->nlocal);

// put extra atom info held by fix back into atom->extra
// destroy temporary fix

//    if (nextra) {
//        memory->destroy(atom->extra);
//        memory->create(atom->extra,atom->nmax,nextra,"atom:extra");
//        int ifix = modify->find_fix("_read_restart");
//        FixReadRestart *fix = (FixReadRestart *) modify->fix[ifix];
//        int *count = fix->count;
//        double **extra = fix->extra;
//        double **atom_extra = atom->extra;
//        int nlocal = atom->nlocal;
//        for (int i = 0; i < nlocal; i++)
//            for (int j = 0; j < count[i]; j++)
//                atom_extra[i][j] = extra[i][j];
//        modify->delete_fix("_read_restart");
//    }



















// check that all atoms were assigned to procs

bigint natoms;
bigint nblocal = lammps->atom->nlocal;
MPI_Allreduce(&nblocal, &natoms, 1, MPI_LMP_BIGINT, MPI_SUM, lammps->world);



if(natoms != lammps->atom->natoms)
lammps->error->all(FLERR,"Did not assign all restart atoms correctly");
MPI_Barrier(MPI_COMM_WORLD);


double walltime;
if(me == 0)
    MPI_Recv(&walltime, 1, MPI_DOUBLE, 0, 55, DMR_INTERCOMM, MPI_STATUS_IGNORE);


double time_send_receive;
if(me == 0) {
    time_send_receive = MPI_Wtime() - walltime;
    std::cout << "Receiving atoms and initialization time in new processes: " << time_send_receive << std::endl;
}


std::fstream fs;
if(me == 0) {
    fs.open("data.txt", std::fstream::in | std::fstream::out | std::fstream::app);
    fs << remote_size << " " << local_size << " " << natoms << " " << time_send_receive << " ";
    fs.close();
}





MPI_Barrier(MPI_COMM_WORLD);
lammps->walltime = MPI_Wtime();
lammps->input->file();

delete lammps;
}





void DMR_LAMMPS::send_expand_cr() {

    double time2;
    if(me == 0)
        time2 = MPI_Wtime();


    if(me == 0) {
        MPI_Send(&lmp->input_argc, 1, MPI_INT, 0, 100, DMR_INTERCOMM);
        for(int i = 0; i < lmp->input_argc; ++i)
            MPI_Send(lmp->input_argv[i], 50, MPI_CHAR, 0, 100, DMR_INTERCOMM);
    }

//    lmp->init();
//    if (domain->triclinic) domain->x2lamda(atom->nlocal);
//    domain->pbc();
//    domain->reset_box();
//    comm->setup();
//    comm->exchange();
//    comm->borders();
//    if (domain->triclinic) domain->lamda2x(atom->nlocal+atom->nghost);

    WriteRestart *restart_dmr = new WriteRestart(lmp);
//    restart_dmr->multiproc_options(nprocs, 0, 0, NULL);
    char *arg[] = {"dmr_restart.%.dat"};
    restart_dmr->command(1, arg);
//    restart_dmr->write("dmr_restart.%.dat");
    MPI_Barrier(DMR_INTERCOMM);

    if(me == 0)
        std::cout << "Send time: " << MPI_Wtime() - time2 << std::endl;
    if(me == 0)
        MPI_Send(&walltime, 1, MPI_DOUBLE, 0, 55, DMR_INTERCOMM);

}
void DMR_LAMMPS::recv_expand_cr() {

}
void DMR_LAMMPS::send_shrink_cr(){

}
void DMR_LAMMPS::recv_shrink_cr(){

}
void initialize_new_cr() {

    double time3;



    int me;
    int remote_size, local_size;
    MPI_Comm DMR_INTERCOMM;
    MPI_Comm_rank(MPI_COMM_WORLD, &me);
    MPI_Comm_size(MPI_COMM_WORLD, &local_size);
    MPI_Comm_get_parent(&DMR_INTERCOMM);
    MPI_Comm_remote_size(DMR_INTERCOMM, &remote_size);

    if(me == 0)
        time3 = MPI_Wtime();

    int input_argc;
    char **input_argv;

    if(me == 0) {
        MPI_Recv(&input_argc, 1, MPI_INT, 0, 100, DMR_INTERCOMM, MPI_STATUS_IGNORE);
    }
    MPI_Bcast(&input_argc, 1, MPI_INT, 0, MPI_COMM_WORLD);
    input_argv = (char **) new char*[input_argc];

    for(int i = 0; i < input_argc; ++i) {
        input_argv[i] = (char*) new char*[50];
        if(me == 0)
            MPI_Recv(input_argv[i], 50, MPI_CHAR, 0, 100, DMR_INTERCOMM, MPI_STATUS_IGNORE);
        MPI_Bcast(input_argv[i], 50, MPI_CHAR, 0, MPI_COMM_WORLD);

    }

    LAMMPS *lammps = new LAMMPS(input_argc, input_argv, MPI_COMM_WORLD);
    lammps->input->dmr_restart_flag = 1;

    ReadRestart *read_restart = new ReadRestart(lammps);
    char *arg[] = {"dmr_restart.%.dat"};
    MPI_Barrier(DMR_INTERCOMM);
    read_restart->command(1, arg);

    double walltime;
    if(me == 0)
        MPI_Recv(&walltime, 1, MPI_DOUBLE, 0, 55, DMR_INTERCOMM, MPI_STATUS_IGNORE);


    double time_send_receive;
    if(me == 0) {
        time_send_receive = MPI_Wtime() - walltime;
        std::cout << "Receiving atoms and initialization time in new processes: " << time_send_receive << std::endl;
    }


    std::fstream fs;
    if(me == 0) {
        fs.open("data.txt", std::fstream::in | std::fstream::out | std::fstream::app);
        fs << remote_size << " " << local_size << " " << lammps->atom->natoms << " " << time_send_receive << " ";
        fs.close();
    }


    MPI_Barrier(MPI_COMM_WORLD);
    lammps->walltime = MPI_Wtime();

    lammps->input->file();

    delete lammps;

}

