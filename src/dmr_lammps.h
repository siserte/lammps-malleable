#ifndef DMR_LAMMPS_H
#define DMR_LAMMPS_H

#include <vector>
#include "pointers.h"


namespace LAMMPS_NS {

class DMR_LAMMPS : protected Pointers
{
public:
    DMR_LAMMPS(LAMMPS *lmp, MPI_Comm &);
    void send_expand();
    void recv_expand();
    void send_shrink();
    void recv_shrink();

    void send_expand_cr();
    void recv_expand_cr();
    void send_shrink_cr();
    void recv_shrink_cr();

    ~DMR_LAMMPS();

    MPI_Comm &DMR_INTERCOMM;
    bigint natoms;
    double walltime;

    int min_procs;
    int max_procs;
    int pref_procs;

private:
    int me;
    int nprocs;
    int pos;
    std::vector<char> buf;
};
}

void initialize_new();
void initialize_new_cr();



#endif // DMR_LAMMPS_H
